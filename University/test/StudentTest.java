import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class StudentTest {
    private static Student student1 = null,
    student2 = null,
    student3 = null,
    student4 = null;

    private static String student1Id = "SZT1",
    student2Id = "S222",
    student3Id = "SAM3",
    student4Id = "SDD4";


    @BeforeAll
    static void setUpAll(){
        System.out.println("TEST WORKS IF YOU RUN EACH FILE SEPARATELY (due to the structure of the TESTS not the code)");
        student1 = new Student("Zane", "Test","#13 Bellview Road, 96th Avenu, California", "superbel@school.org");
        student2 = new Student("2Zane", "2Test", "4598 Mount Tabor, West Nyack ,New York", "carladrum@h.hat");
        student3 = new Student("Arcane", "Mest","4313 Edwards Street Robersonville, NC 27871", "JosephFRobinson@teleworm.us");
        student4 = new Student("Dr", "Dinnertime", "3421 James Street Rochester, NY 14621", "GinaFAnderson@armyspy.com");
    }

    @AfterAll
    static void tearDownAll(){
        student1 = null;
        student2 = null;
        student3 = null;
        student4 = null;
    }
//
//    @BeforeEach
//    void setUp() {
//    }
//
//    @AfterEach
//    void tearDown() {
//        setStudentI= null;
//    }

    @Test
    void setStudentId1() {
        System.out.println("setStudentId1");
//        setStudentI= new Student("Zane", "Test");
        String expectedResult = student1Id;
        String result = student1.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setStudentId2() {
        System.out.println("setStudentId2");
//        setStudentI= new Student("2Zane", "2Test");
        String expectedResult = student2Id;
        String result = student2.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setStudentId3() {
        System.out.println("setStudentId3");
//        setStudentI= new Student("Arcane", "Mest");
        String expectedResult = student3Id;
        String result = student3.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setStudentId4() {
        System.out.println("setStudentId4");
//        setStudentI= new Student("Dr", "Dinnertime");
        String expectedResult = student4Id;
        String result = student4.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setStudentId1NotEquals() {
        System.out.println("setStudentId1NotEquals");
//        setStudentI= new Student("Zane", "Test");
        String id = student1Id;

        assertNotEquals(student2.getId(), id);
        assertNotEquals(student3.getId(), id);
        assertNotEquals(student4.getId(), id);
    }

    @Test
    void setStudentId2NotEquals() {
        System.out.println("setStudentId2NotEquals");

        String id = student2Id;

        assertNotEquals(student1.getId(), id);
        assertNotEquals(student3.getId(), id);
        assertNotEquals(student4.getId(), id);
    }

    @Test
    void setStudentId3NotEquals() {
        System.out.println("setStudentId3NotEquals");

        String id = student3Id;

        assertNotEquals(student1.getId(), id);
        assertNotEquals(student2.getId(), id);
        assertNotEquals(student4.getId(), id);
    }

    @Test
    void setStudentId4NotEquals() {
        System.out.println("setStudentId4NotEquals");

        String id = student4Id;

        assertNotEquals(student1.getId(), id);
        assertNotEquals(student3.getId(), id);
        assertNotEquals(student2.getId(), id);
    }

}