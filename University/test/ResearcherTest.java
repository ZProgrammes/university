import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ResearcherTest {
    private static Researcher researcher1 = null,
            researcher2 = null,
            researcher3 = null,
            researcher4 = null;

    private static String researcher1Id = "RZZ1",
            researcher2Id = "R922",
            researcher3Id = "RAR3",
            researcher4Id = "RDD4";


    @BeforeAll
    static void setUpAll(){
        System.out.println("TEST WORKS IF YOU RUN EACH FILE SEPARATELY (due to the structure of the TESTS not the code)");
        researcher1 = new Researcher("Zachery", "Zone",12,  "4563 Lawman Avenue Dulles, VA 20166", "MadeleinePLabelle@dayrep.com");
        researcher2 = new Researcher("9Kal", "2El", 34, "1843 Prospect Valley Road Irvine, CA 92614", "JamesDNovak@jourrapide.com");
        researcher3 = new Researcher("Artemis", "Romeo", 45, "1679 Elk Street Laguna Niguel, CA 92677", "JudithEJenkins@teleworm.us");
        researcher4 = new Researcher("Dr", "Doofenshmirtz",  8, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");
        Researcher.researcherCount = 0;
    }

    @AfterAll
    static void tearDownAll(){
        researcher1 = null;
        researcher2 = null;
        researcher3 = null;
        researcher4 = null;
    }


    @Test
    void setResearcherId1() {
        System.out.println("setResearcherId1");

        String expectedResult = researcher1Id;
        String result = researcher1.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setResearcherId2() {
        System.out.println("setResearcherId2");

        String expectedResult = researcher2Id;
        String result = researcher2.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setResearcherId3() {
        System.out.println("setResearcherId3");

        String expectedResult = researcher3Id;
        String result = researcher3.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setResearcherId4() {
        System.out.println("setResearcherId4");

        String expectedResult = researcher4Id;
        String result = researcher4.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setResearcherId1NotEquals() {
        System.out.println("setResearcherId1NotEquals");

        String id = researcher1Id;

        assertNotEquals(researcher2.getId(), id);
        assertNotEquals(researcher3.getId(), id);
        assertNotEquals(researcher4.getId(), id);
    }

    @Test
    void setResearcherId2NotEquals() {
        System.out.println("setResearcherId2NotEquals");

        String id = researcher2Id;

        assertNotEquals(researcher1.getId(), id);
        assertNotEquals(researcher3.getId(), id);
        assertNotEquals(researcher4.getId(), id);
    }

    @Test
    void setResearcherId3NotEquals() {
        System.out.println("setResearcherId3NotEquals");

        String id = researcher3Id;

        assertNotEquals(researcher1.getId(), id);
        assertNotEquals(researcher2.getId(), id);
        assertNotEquals(researcher4.getId(), id);
    }

    @Test
    void setResearcherId4NotEquals() {
        System.out.println("setResearcherId4NotEquals");

        String id = researcher4Id;

        assertNotEquals(researcher1.getId(), id);
        assertNotEquals(researcher3.getId(), id);
        assertNotEquals(researcher2.getId(), id);
    }
}