import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sound.midi.Soundbank;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class AppointmentTest {
    private SimpleDateFormat appointmentDateFormat = new SimpleDateFormat("EEEE d MMMM hh:mm a");
    private Researcher appointmentMaker = new Researcher("Karter", "Cane", 23, "#12 Kingston St., Manhattan", "carterpharaoh@kanenetwork.egypt");
    private Teacher appointmentMadeToSee = new Teacher("Professor", "Heinz", "Doofenshmirtz", 84, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");

    private Appointment appointment1 = new  Appointment(appointmentMaker, appointmentMadeToSee, appointmentDateFormat.parse("Saturday 24 October 3:00 PM"), appointmentMadeToSee.getOfficeNumber());;
    private Appointment appointment2 = new Appointment(appointmentMaker, appointmentMadeToSee, appointmentDateFormat.parse("Monday 22 October 10:00 AM"), appointmentMadeToSee.getOfficeNumber());;

    AppointmentTest() throws ParseException {
    }

    @Test
    void test_incrementingID() {
        System.out.println("TEST WORKS IF YOU RUN EACH FILE SEPARATELY (due to the structure of the TESTS not the code)");
        System.out.println("test_incrementingID");

        assertEquals(1, appointment1.getId());
        assertEquals(2, appointment2.getId());
    }
}