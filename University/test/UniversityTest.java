import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class UniversityTest {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE hh:mm a");
    private University university = University.getInstance();

    @BeforeEach
    void setUp() {
        university.getStaff().clear();
        Teacher.teacherCount = 0;
        university.getClassrooms().clear();
        Researcher.researcherCount = 0;
        Student.studentCount = 0;
        Appointment.count = 0;
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addTeacher() {
        System.out.println("TEST WORKS IF YOU RUN EACH FILE SEPARATELY (due to the structure of the TESTS not the code)");
        System.out.println("addTeacher");

        university.addTeacher("Professor", "Heinz", "Doofenshmirtz", 84, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");

        assertEquals(1, university.getStaff().size()); // Assert that something was added to array.
        assertTrue(university.getStaff().containsKey("THD1"));  // Assert that the key is in the array.
        assertEquals("THD1", university.getStaff().get("THD1").getId());  // Assert that the object at the key is the correct one by checking the id.
    }

    @Test
    void addStudent() {
        System.out.println("addStudent");

        university.addStudent("Hudson", "Belpois", "#13 Bellview Road, 96th Avenu, California", "superbel@school.org");

        assertEquals(1, university.getStudents().size());
        assertTrue(university.getStudents().containsKey("SHB1"));
        assertEquals("SHB1", university.getStudents().get("SHB1").getId());
    }

    @Test
    void addResearcher() {
        System.out.println("addResearcher");

        university.addResearcher("Royal", "Kingsly", 30, "221B Barker St., New York", "kingroyal@highretek.com");

        assertEquals(1, university.getStaff().size());
        assertTrue(university.getStaff().containsKey("RRK1"));
        assertEquals("RRK1", university.getStaff().get("RRK1").getId());
    }

    @Test
    void addClassroom() {
        System.out.println("addClassroom");

        university.addClassroom("A12", 12);

        assertEquals(1, university.getClassrooms().size());
        assertTrue(university.getClassrooms().containsKey("A12"));
        assertEquals("A12", university.getClassrooms().get("A12").getName());
    }

    @Test
    void addCourse() throws ParseException {
        System.out.println("addCourse");

        university.addTeacher("Professor", "Heinz", "Doofenshmirtz", 84, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");
        university.addClassroom("A12", 12);

        university.addCourse("PH91", university.getTeacherById("THD1"), university.getClassroomByName("A12"),
                "Physics", "Electromagnetism and Optics",dateFormat.parse("Thursday 4:00 PM"), dateFormat.parse("Thursday 7:00 PM"));

        assertEquals(1, university.getCourses().size());
        assertTrue(university.getCourses().containsKey("PH91"));
        assertEquals("PH91", university.getCourses().get("PH91").getCourseId());
    }

    @Test
    void addAvailableAppointmentDate() throws ParseException {
        System.out.println("addAvailableAppointmentDate");

        university.addTeacher("Professor", "Heinz", "Doofenshmirtz", 84, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");


        assertTrue(university.addAvailableAppointmentDate("THD1", dateFormat.parse("Thursday 4:00 PM")));
        assertFalse(university.addAvailableAppointmentDate("THD1", dateFormat.parse("Thursday 4:00 PM")));

        assertEquals(1, university.getAvailableAppointmentDates().size());
        assertTrue(university.getAvailableAppointmentDates().containsKey("THD1"));
        assertTrue(university.getAvailableAppointmentDates().get("THD1").contains(dateFormat.parse("Thursday 4:00 PM")));
    }

    @Test
    void addAppointment() throws ParseException {
        System.out.println("addAppointment");

        university.addResearcher("Royal", "Kingsly", 30, "221B Barker St., New York", "kingroyal@highretek.com");
        university.addTeacher("Professor", "Heinz", "Doofenshmirtz", 84, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");
        university.addAvailableAppointmentDate("THD1", dateFormat.parse("Thursday 4:00 PM"));

        assertEquals("true",university.addAppointment(university.getResearcherById("RKK1"), university.getTeacherById("THD1"), dateFormat.parse("Thursday 4:00 PM"))[0]);

        assertEquals(1, university.getAppointments().size());
        assertTrue(university.getAppointments().containsKey("1"));
        assertTrue(university.getAvailableAppointmentDates().get("THD1").contains(dateFormat.parse("Thursday 4:00 PM")));
    }

}