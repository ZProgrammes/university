import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class CourseTest {
    private SimpleDateFormat sdf = new SimpleDateFormat("EEEE hh:mm a");
    private Teacher teacher;
    private Classroom tempRoom;
   /* private Student student1 = new Student("James", "Bond", "The car int he Cave", "DO1@doublecross.com");
    private Student student2 = new Student("Yearl", "McDonald", "Not here for Sure", "space@alien.zorg");
    private Student student3 = new Student("Scrooge", "McDuck", "The biggest House, Duckburg", "sd@richie.rich");*/

    private Course course1;

    @BeforeEach
    void setUp() throws ParseException {
        teacher = new Teacher("Lecturer", "Jack", "Jill",234, "Down the Road", "hello@byebye.com");
        tempRoom = new Classroom("Z0", 2);

        course1 = new Course("M345", teacher, tempRoom, "Mathematics", "Advanced Mathematics",
                sdf.parse("Monday 12:00 AM"), sdf.parse("Monday 3:00 PM"));
    }

    @AfterEach
    void tearDown() {
        teacher = null;
        tempRoom = null;
        course1 = null;
    }

    /*@Test
    void registerStudent_AddedToList() {
        System.out.println("registerStudent_AddedToList: \n\tone can be successfully registered");

        assertTrue(course1.registerStudent(student1));
    }

    @Test
    void registerStudent_1ContainedInList() {
        System.out.println("registerStudent_1ContainedInList: \n\tthe size of the list == 1 when on student is registered");

        course1.registerStudent(student1);

        assertTrue(course1.getStudents().contains(student1));
        assertEquals(1, course1.getStudents().size());
    }

    @Test
    void registerStudent_CantPassFull() {
        System.out.println("registerStudent_CantPassFull: \n\ta third student cannot be added to the list");

        assertTrue(course1.registerStudent(student1));
        assertTrue(course1.registerStudent(student2));
        assertFalse(course1.registerStudent(student3));

        assertNotEquals(3, course1.getStudents().size());
    }*/

    @Test
    void testClassroomIssetOccupied() {
        System.out.println("TEST WORKS IF YOU RUN EACH FILE SEPARATELY (due to the structure of the TESTS not the code)");
        System.out.println("testClassroomIssetOccupied");

        assertEquals("occupied", course1.getClassroom().getStatus());
    }

    @Test
    void testClassroomIsnotsetUnoccupied() {
        System.out.println("testClassroomIssetOccupied");

        assertNotEquals("unoccupied", course1.getClassroom().getStatus());
    }
}