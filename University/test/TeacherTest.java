import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class TeacherTest {
    private static Teacher teacher1 = null,
    teacher2 = null,
    teacher3 = null,
    teacher4 = null;

    private static String teacher1Id = "TZS1",
    teacher2Id = "TRT2",
    teacher3Id = "TAH3",
    teacher4Id = "TSR4";


    @BeforeAll
    static void setUpAll() {
        System.out.println("TEST WORKS IF YOU RUN EACH FILE SEPARATELY (due to the structure of the TESTS not the code)");
        teacher1 = new Teacher("Professor","Zack", "Stan", 12, "749 Chipmunk Lane Portland, ME 04101", "zs@hotmial.com");
        teacher2 = new Teacher("Professor","Rats", "Tzest", 23, "3185 Ashcraft Court San Diego, CA 92103", "ratchatcher@rats.com");
        teacher3 = new Teacher("Lecturer","Achilles", "Hudson", 34, "3580 Emeral Dreams Drive Nelson, IL 61058", "heel@olympus.power");
        teacher4 = new Teacher("Lecturer","Sir Terry", "Ratchet", 45, "360 Maud Street Philadelphia, DE 19103", "thefirstknightteacher@kingdomknights.com");
        Teacher.teacherCount = 0;
    }

    @AfterAll
    static void tearDownAll() {
        teacher1 = null;
        teacher2 = null;
        teacher3 = null;
        teacher4 = null;
    }

    /*------------------------------Assert Equals------------------------------*/
    /**
     * Test that the id creation for the class is working(admitidly not the best way to do it;was just trying something out)
     * */
    @Test
    void setTeacherId1() {
        System.out.println("setTeacherId1");
//        teacher = new Student("Zane", "Test");
        String expectedResult = teacher1Id;
        String result = teacher1.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setTeacherId2() {
        System.out.println("setTeacherId2");
//        teacher = new Student("2Zane", "2Test");
        String expectedResult = teacher2Id;
        String result = teacher2.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setTeacherId3() {
        System.out.println("setTeacherId3");
//        teacher = new Student("Arcane", "Mest");
        String expectedResult = teacher3Id;
        String result = teacher3.getId();
        assertEquals(expectedResult, result);
    }

    @Test
    void setTeacherId4() {
        System.out.println("setTeacherId4");
//        teacher = new Student("Dr", "Dinnertime");
        String expectedResult = teacher4Id;
        String result = teacher4.getId();
        assertEquals(expectedResult, result);
    }

    /*------------------------------Assert Not Equals------------------------------s*/
    @Test
    void setTeacherId1NotEquals() {
        System.out.println("setTeacherId1NotEquals");

        String id = teacher1Id;

        String expectedResult2 = teacher2Id;
        String result2 = teacher2.getId();
        assertEquals(expectedResult2, result2);
    }


}