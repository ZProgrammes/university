import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

/*FIXME create JUnit test for this class after creating full GUI*/
//Will contain the lists of Students, Teachers, etc...
public class University implements UniversityInterface {
    private static University instance = new University();
    private HashMap<String, Classroom> classrooms = new HashMap<>();
    private HashMap<String, Course> courses = new HashMap<>();
    private HashMap<String, Student> students = new HashMap<>();
    private HashMap<String, UniversityMember> staff = new HashMap<>();
    private HashMap<String, Appointment> appointments = new HashMap<>();
    private HashMap<String, ArrayList<Date>> availableAppointmentDates = new HashMap<>();
    private ArrayList<StudentSchedule> studentSchedules = new ArrayList<StudentSchedule>(){
        @Override
        public boolean add(StudentSchedule studentSchedule) {
            courses.get(studentSchedule.getCourseId()).getClassroom().setOccupiedSeats();  // Automatically increase occupied seats
            return super.add(studentSchedule);
        }
    };
    private String adminPassword;

    private University() {
    }

    public static synchronized University getInstance() {
        return instance;
    }

    @Override
    public void addTeacher(String title, String firstName, String lastName, int officeNumber, String address, String email) {
        Teacher teacher = new Teacher(title, firstName, lastName, officeNumber, address, email);
        staff.put(teacher.getId(), teacher);
    }

    @Override
    public void addStudent(String firstName, String lastName, String address, String email) {
        Student student = new Student(firstName, lastName, address, email);
        students.put(student.getId(), student);
    }

    @Override
    public void addResearcher(String firstName, String lastName, int officeNumber, String address, String email) {
        Researcher researcher = new Researcher(firstName, lastName, officeNumber, address, email);
        staff.put(researcher.getId(), researcher);
    }

    @Override
    public void addClassroom(String name, int maxSeats) {
        Classroom classroom = new Classroom(name, maxSeats);
        classrooms.put(classroom.getName(), classroom);
    }

    @Override
    public boolean addCourse(String courseId, Teacher teacher, Classroom classroom, String subject, String name, Date startTime, Date endTime) {
        try {
            if (teacher.getCourseCount() < teacher.getMaxClasses()) {
                Course aCourse = new Course(courseId, teacher, classroom, subject, name, startTime, endTime);
                courses.put(aCourse.getCourseId(), aCourse);
                return true;
            }
        } catch (NullPointerException e) {
            System.out.println("Either the Teacher or Classroom entered does not exist or its id/name is misspelled.");
            e.printStackTrace();
        }
        return false;
    }



    @Override
    public boolean addAvailableAppointmentDate(String staffId, Date date) {

        if (staff.containsKey(staffId)) {  // If teacher exists.
            if (availableAppointmentDates.containsKey(staffId)) { // If staff has already set available dates before.

                if (!availableAppointmentDates.get(staffId).contains(date)) { //Check if the date has not been created before.
                    availableAppointmentDates.get(staffId).add(date);
                    return true;
                }
            } else {
                availableAppointmentDates.put(staffId, new ArrayList<>(Arrays.asList(date))); // If staff has NOT already set available dates before.
                return true;
            }
        }

        return false;
    }

    @Override
    public String[] addAppointment(User makerOfAppointment, UniversityMember madeAppointmentToSee, Date time) {
        String[] results = new String[2];
        Appointment appointment = new Appointment(makerOfAppointment, madeAppointmentToSee, time, madeAppointmentToSee.getOfficeNumber());

        if (availableAppointmentDates.containsKey(madeAppointmentToSee.getId())) { // If staff has a list of available dates:
            if (availableAppointmentDates.get(madeAppointmentToSee.getId()).contains(time)) { // If one of the dates == the given time:
                if (!appointments.containsValue(appointment)) { // Check that the appointment has not already been made:
                    appointments.put(Integer.toString(appointment.getId()), appointment); // Add the appointment to appointments.

                    results[0] = "true";
                    results[1] = Integer.toString(appointment.getId());
                } else {
                    results[0] = "false";
                    results[1] = "This appointment has already been made.";
                }
            } else {
                results[0] = "false";
                results[1] = "The staff member you are trying to see\nis not available on this day.";
            }
        } else {
            results[0] = "false";
            results[1] = "There are no available dates for this staff member.";
        }

        return results;
    }

    @Override
    public String[] registerStudent(String courseId, Student student) {
        String[] results = new String[2];

        if (!courses.containsKey(courseId)) { // Course does not exist.
//            results.addAll  ({{"false", "Course does not exist!!"}});
            results[0] = "false"; results[1] = "Course does not exist!!";
            return results;
        } else if (!students.containsKey(student.getId())) { // Student does not exist.
            results[0] = "false"; results[1] = "Student does not exist!!";
            return results;
        } else {
            for (StudentSchedule schedule : studentSchedules) {
                if (schedule.getStudent() == student) { //Check if student has any previous schedules.

                    if (schedule.getCourseId().equals(courseId)) { // if student is already registered for this course.
                        results[0] = "false"; results[1] = "Already registered for this course!";
                        return results;

                    } else if (checkForTimeClash(courses.get(schedule.getCourseId()), courses.get(courseId))) { // If course times do clash.
                        results[0] = "false"; results[1] = "The timing for the course \"" + courses.get(schedule.getCourseId()).getName() + "\" clashes with that of \"" + courses.get(courseId).getName() + "\".";
                        return results;

                    } else if (!(courses.get(courseId).getClassroom().getOccupiedSeats() < courses.get(courseId).getClassroom().getMaxSeats())) { // If there is no available seats
                            results[0] = "false"; results[1] = "No available seats!";
                            return results;
                    }
                }
            }
        }
        // no if statements triggered and the hasMap not returned as yet create & add studentSchedule and increment seat count.
        studentSchedules.add(new StudentSchedule(courseId, student)); // Occupied seats is automatically increased within my modified `add` method defined above.
        results[0] = "true"; results[1] = "Successfully Registered for the course \"" + courses.get(courseId).getName() + "\" !";
        return results;
        }

    @Override
    public String listAppointments(String staffId) {
        StringBuilder builder = new StringBuilder();

        for(Appointment appointment : appointments.values()){
            if (appointment.getMadeAppointmentToSee().getId().equalsIgnoreCase(staffId) ||
                    (appointment.getMakerOfAppointment() instanceof UniversityMember && ((UniversityMember) appointment.getMakerOfAppointment()).getId().equalsIgnoreCase(staffId))) {
                builder.append(appointment);
            }
        }

        return builder.toString();
    }
    //region Searchers


    @Override
    public Course getCourseById(String id) {
        return courses.get(id);
    }

    @Override
    public Classroom getClassroomByName(String name) {
        return classrooms.get(name);
    }

    @Override
    public Teacher getTeacherById(String id) {
        return (Teacher) staff.get(id);
    }

    @Override
    public Teacher getTeacherByName(String name) {
        for (UniversityMember member : staff.values()) {
            if ((member.getFirstName() + " " + member.getLastName()).equalsIgnoreCase(name)) {
                return (Teacher) member;
            }
        }
        return null;
    }

    @Override
    public UniversityMember getStaffByName(String name) {
        for (UniversityMember member : staff.values()) {
            if ((member.getFirstName() + " " + member.getLastName()).equalsIgnoreCase(name)) {
                return member;
            }
        }
        return null;
    }

    @Override
    public Researcher getResearcherById(String id) {
        return (Researcher) staff.get(id);
    }

    @Override
    public Student getStudentById(String id) {
        return students.get(id);
    }

    @Override
    public ArrayList<Course> searchClasses(String criteria, String value) {
        ArrayList<Course> searchedCourses = new ArrayList<>();

        if (criteria.equalsIgnoreCase("Name")) {

            for (Course course : courses.values()) {
                if (course.getName().toLowerCase().contains(value.toLowerCase()) ||
                        (course.getName().toLowerCase().startsWith(value.toLowerCase())) ||
                        (course.getName().toLowerCase().endsWith(value.toLowerCase()))) {

                    if (course.getClassroom().getMaxSeats() > course.getClassroom().getOccupiedSeats()) { // Only add show if there are available seats.
                        searchedCourses.add(course);
                    }
                }

            }

        } else if (criteria.equalsIgnoreCase("Expertise")) {

            for (Course course : courses.values()) {
                if (course.getSubject().equalsIgnoreCase(value)) {
                    if (course.getClassroom().getMaxSeats() > course.getClassroom().getOccupiedSeats()) { // Only add show if there are available seats.
                        searchedCourses.add(course);
                    }
                }
            }

        } else if (criteria.equalsIgnoreCase("Teacher")) {

            for (Course course : courses.values()) {
                String teacherName = course.getTeacher().getFirstName().toLowerCase() + " " + course.getTeacher().getLastName().toLowerCase();
                if (teacherName.contains(value.toLowerCase()) ||
                        (teacherName.startsWith(value.toLowerCase())) ||
                        (teacherName.endsWith(value.toLowerCase()))) {

                    if (course.getClassroom().getMaxSeats() > course.getClassroom().getOccupiedSeats()) { // Only add show if there are available seats.
                        searchedCourses.add(course);
                    }
                }

            }

        }

        return searchedCourses;
    }

    //endregion

    @Override
    public UniversityMember checkExistId(String type, String id) {
        if (type.equalsIgnoreCase("Student")) {
            return students.get(id);
        } else if (type.equalsIgnoreCase("Teacher")) {
            return staff.get(id);
        } else if (type.equalsIgnoreCase("Researcher")) {
            return staff.get(id);
        }
        return null;
    }

    @Override
    public boolean checkForTimeClash(Course course1, Course course2) {
        //Returns 'true' if the courses clash
        if (course2.getStartTime().before(course1.getStartTime()) && (course2.getEndTime().after(course1.getStartTime())))
            return true;
        else if (course2.getStartTime().after(course1.getStartTime()) && (course2.getStartTime().before(course1.getEndTime())))
            return true;
        else if (course2.getEndTime().equals(course1.getStartTime()) || (course2.getEndTime().equals(course1.getEndTime())))
            return true;
        else
            return course2.getStartTime().equals(course1.getEndTime()) || (course2.getStartTime().equals(course1.getStartTime()));
    }

    @Override
    public boolean removeMember(String id) {
        if (staff.containsKey(id)) {
            staff.remove(id);
        } else if (students.containsKey(id)) {
            students.remove(id);
        } else return false;

        return true;
    }

    //region Getters &  Setters

    public HashMap<String, Classroom> getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(HashMap<String, Classroom> classrooms) {
        this.classrooms = classrooms;
    }

    public HashMap<String, Course> getCourses() {
        return courses;
    }

    public void setCourses(HashMap<String, Course> courses) {
        this.courses = courses;
    }

    public HashMap<String, Student> getStudents() {
        return students;
    }

    public void setStudents(HashMap<String, Student> students) {
        this.students = students;
    }

    public HashMap<String, UniversityMember> getStaff() {
        return staff;
    }

    public void setStaff(HashMap<String, UniversityMember> staff) {
        this.staff = staff;
    }

    public HashMap<String, Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(HashMap<String, Appointment> appointments) {
        this.appointments = appointments;
    }

    public HashMap<String, ArrayList<Date>> getAvailableAppointmentDates() {
        return availableAppointmentDates;
    }

    public void setAvailableAppointmentDates(HashMap<String, ArrayList<Date>> availableAppointmentDates) {
        this.availableAppointmentDates = availableAppointmentDates;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public ArrayList<StudentSchedule> getStudentSchedules() {
        return studentSchedules;
    }

    public void setStudentSchedules(ArrayList<StudentSchedule> studentSchedules) {
        this.studentSchedules = studentSchedules;
    }

    //endregion
}
