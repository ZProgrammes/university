public class Researcher extends UniversityMember implements User{
    static int researcherCount;
    private int officeNumber;

    Researcher(String firstName, String lastName, int officeNumber, String address, String email) {
        super(firstName, lastName, address, email);
        this.officeNumber = officeNumber;
    }

    @Override
    String createId() {
        researcherCount++; // Increase the var. studentCount by one.
        // Sets the id to SFL2 OR TFL2  [T-teacher, S-student, F-fname, L-lname) (Id nums start at 1]
        return "" + 'R' + this.getFirstName().charAt(0) + this.getLastName().charAt(0) + researcherCount;
    }


    public int getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(int officeNumber) {
        this.officeNumber = officeNumber;
    }
}
