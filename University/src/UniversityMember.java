public abstract class UniversityMember implements User{
    private String firstName, lastName;
    private String id;
    private String address, email;

    UniversityMember(String firstName, String lastName, String address, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.email = email;
        setId(createId());
    }

    UniversityMember(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = "Enter address";
        this.email = "Enter Email";
        setId(createId());
    }

    abstract String createId();

    public int getOfficeNumber() {
        return 0;
    }

    /*------------------------------Getters and Setters------------------------------*/
    public String getId() {
        return id;
    }

    public void setId(String id) { 
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
