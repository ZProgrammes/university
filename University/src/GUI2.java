import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class GUI2 extends JFrame implements ActionListener {
    private University university = University.getInstance();

    //region Student's menu bar
    private JMenuBar menuBarStudent;
    private JMenu search;
    private JMenuItem searchClassesByName, searchClassesByTeacher, math, physics, dataAnalysis, programming;
    //endregion

    //region Staff's menu bar
    private JMenuBar menuBarGeneral;
    private JMenu appointmentsMenu;
    private JMenuItem staffListAppointments, staffAddAvailableAppointmentDates;
    //endregion

    //region Researcher Menu item.
    private JMenuItem researcherMakeAppointment;
    //endregion

    //region make Appointment panel and components.
    private JPanel makeAppointmentPanel;
    private JTextField makeAppointmentFirstNameField, makeAppointmentLastNameField, makeAppointmentEmailField, makeAppointmentAddressField;
    private JComboBox<String> chooseStaffDropdown;
    private JButton researcherCloseMakeAppointmentPanelBtn, researcherMakeAppointmentBtn, visitorMakeAppointmentBtn, visitorCloseMakeAppointmentPanelBtn;
    //endregion

    //region Login Panel and sub-panels
    private JPanel loginPanel;
    private JButton studentBtn, teacherBtn, researcherBtn, visitorBtn, adminBtn;
    //endregion

    //region Details Panel and contained objects
    private JPanel memberDetailsPanel;
    private JTextField memberIdTxtFiled, firstNameTxtField, lastNameTxtField, emailTxtField, addressTxtField, positionTxtField;
    private JButton exitBtn;
    //endregion

    //region Search Classes by Name
    private JPanel searchedClassesPanel;
    private DefaultTableModel searchedClassesModel;
    private JTable searchedClassesTable;
    private JButton closeSearchedClassesPanel;
    //endregion

    //region Class details Panel
    private JPanel classDetailsPanel;
    private JLabel classNameLabel;
    private JTextField classIdTxtField, classTeacherNameTxtField, classCategoryTxtField, availableSeatsTxtField, classRoomTxtField, classDayTxtField,
            classStartsTxtField, classEndsTxtField;
    private JButton registerBtn, classDetailsBackBtn;
    //endregion

    //region List Appointments panel
    private JPanel listAppointmentsPanel;
    private JTextPane listAppointments_txtPane;
    private JButton closeListAppointmentsPanelBtn;
    //endregion

    //region Add Available Appointment Dates Panel
    private JPanel addAvailableAppointmentDatesPanel;
    private JTextPane listDates_txtPane;
    private JButton closeAddAvailableAppointmentDatesPanelBtn, addAvailableDateButton;
    private JComboBox<String> availableDatesDay, availableDatesDates, availableDatesMonths, availableDatesTime, availableDatesPeriod;
    private SimpleDateFormat formatAppointmentDate = new SimpleDateFormat("EEEE d MMMM hh:mm a");
    //endregion

    //region Visitor panel
    private JPanel visitorWelcomePanel;
    private JButton exitVisitorBtn;
    //endregion

    //region Visitor menu item for Appointment making.
    private JMenuItem visitorMakeAppointment;
    //endregion

    //region Admin's Menu Bar
    private JMenuBar adminMenuBar;
    private JMenu adminStaffMenu, adminStudentMenu;
    private JMenuItem adminStaffAdd, adminStaffRemove, adminStaffReport,
            adminStudentAdd, adminStudentRemove, adminStudentReport;
    //endregion

    //region Administrator landing panel.
    private JPanel adminPanel;
    private JButton adminExitButton;
    //endregion

    //region Remove Member Panel.
    private JPanel adminRemoveMemberPanel;
    private JTable adminRemoveMembersTable;
    private JButton closeAdminRemoveMemberPanelBtn, adminRemoveMemberBtn;
    private DefaultTableModel adminRemoveMembersModel;
    //endregion

    //region staff Report Panel
    private JPanel memberReportPanel;
    private JTextPane memberReportPane;
    private JButton closeMemberReportPanelBtn, printMemberReportBtn;
    //endregion

    GUI2() {
//        super("University of Unknown");
        setTitle("University of Freeport");
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 600);
        setResizable(false);

        add(loginPanel()); // add the login panel with all other sub-panels to frame.
        add(memberDetailsPanel());
        add(searchedClassesPanel());
        add(classDetailsPanel());
        add(listAppointmentsPanel());
        add(addAvailableAppointmentDatesPanel());
        add(makeAppointmentPanel());
        add(visitorWelcomePanel());
        add(adminPanel());
        add(adminRemoveMemberPanel());
        add(memberReportPanel());
    }


    private JPanel loginPanel() {
        //region Login page.

        loginPanel = new JPanel(new BorderLayout());
        loginPanel.setVisible(true);
        loginPanel.setBounds(0, getHeight() / 3, getWidth(), getHeight() / 2);

        JLabel loginLabel = new JLabel("<html><h1>Are you a...</h1></html>", JLabel.CENTER);
        loginPanel.add(loginLabel, BorderLayout.NORTH);

        JPanel loginPanelInner = new JPanel();
        loginPanel.add(loginPanelInner, BorderLayout.CENTER);

        //<editor-fold desc="All Panels inside of login panel">
        JPanel studentLoginPanel = new JPanel(new BorderLayout());
        studentLoginPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel teacherLoginPanel = new JPanel(new BorderLayout());
        teacherLoginPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel researcherLoginPanel = new JPanel(new BorderLayout());
        researcherLoginPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel visitorLoginPanel = new JPanel(new BorderLayout());
        visitorLoginPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel adminLoginPanel = new JPanel(new BorderLayout());
        adminLoginPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        loginPanelInner.add(studentLoginPanel);
        loginPanelInner.add(teacherLoginPanel);
        loginPanelInner.add(researcherLoginPanel);
        loginPanelInner.add(visitorLoginPanel);
        loginPanelInner.add(adminLoginPanel);
        //</editor-fold>


        /*Object inside of studentLoginPanel*/
        studentBtn = new JButton("Student");
        studentBtn.addActionListener(this);

        studentLoginPanel.add(studentBtn, BorderLayout.NORTH);
        /*END*/


        /*Items inside of teacherLoginPanel*/
        teacherBtn = new JButton("Teacher");
        teacherBtn.addActionListener(this);

        teacherLoginPanel.add(teacherBtn, BorderLayout.NORTH);
        /*END*/


        /*Items inside of researcherLoginPanel*/
        researcherBtn = new JButton("Researcher");
        researcherBtn.addActionListener(this);

        researcherLoginPanel.add(researcherBtn, BorderLayout.NORTH);
        /*END*/


        /*Items inside of visitorLoginPanel*/
        visitorBtn = new JButton("Visitor");
        visitorBtn.addActionListener(this);

        visitorLoginPanel.add(visitorBtn, BorderLayout.NORTH);
        /*END*/

        /*Items inside of adminLoginPanel*/
        adminBtn = new JButton("Admin");
        adminBtn.addActionListener(this);

        adminLoginPanel.add(adminBtn, BorderLayout.NORTH);
        /*END*/

        return loginPanel;
    }


    private JMenuBar menuBarAdmin() {
        adminMenuBar = new JMenuBar();

        adminStaffMenu = new JMenu("Staff");
        adminStaffAdd = new JMenuItem("Add Staff");
        adminStaffAdd.addActionListener(this);
        adminStaffRemove = new JMenuItem("Remove Staff");
        adminStaffRemove.addActionListener(this);
        adminStaffReport = new JMenuItem("Staff Report");
        adminStaffReport.addActionListener(this);
        adminStaffMenu.add(adminStaffAdd);
        adminStaffMenu.add(adminStaffRemove);
        adminStaffMenu.add(adminStaffReport);
        adminMenuBar.add(adminStaffMenu);

        adminStudentMenu = new JMenu("Student");
        adminStudentAdd = new JMenuItem("Add Student");
        adminStudentAdd.addActionListener(this);
        adminStudentRemove = new JMenuItem("Remove Student");
        adminStudentRemove.addActionListener(this);
        adminStudentReport = new JMenuItem("Student Report");
        adminStudentReport.addActionListener(this);
        adminStudentMenu.add(adminStudentAdd);
        adminStudentMenu.add(adminStudentRemove);
        adminStudentMenu.add(adminStudentReport);
        adminMenuBar.add(adminStudentMenu);
        return adminMenuBar;
    }

    private JMenuBar menuBarStudent() {
        menuBarStudent = new JMenuBar();

        search = new JMenu("Search");
        menuBarStudent.add(search);


        JMenu classes = new JMenu("Available Classes");
        search.add(classes);

        searchClassesByName = new JMenuItem("By Name");
        searchClassesByName.addActionListener(this);
        classes.add(searchClassesByName);


        JMenu searchClassesByCategory = new JMenu("By Expertise");
        classes.add(searchClassesByCategory);

        physics = new JMenuItem("Physics");
        physics.addActionListener(this);
        searchClassesByCategory.add(physics);

        math = new JMenuItem("Mathematics");
        math.addActionListener(this);
        searchClassesByCategory.add(math);

        dataAnalysis = new JMenuItem("Data Analysis");
        dataAnalysis.addActionListener(this);
        searchClassesByCategory.add(dataAnalysis);

        programming = new JMenuItem("Programming");
        programming.addActionListener(this);
        searchClassesByCategory.add(programming);


        searchClassesByTeacher = new JMenuItem("By Teacher");
        searchClassesByTeacher.addActionListener(this);
        classes.add(searchClassesByTeacher);


        return menuBarStudent;
    }

    private JMenuBar menuBarStaff() {
        menuBarGeneral = new JMenuBar();

        appointmentsMenu = new JMenu("Appointments");
        menuBarGeneral.add(appointmentsMenu);

        staffListAppointments = new JMenuItem("List Appointments");
        staffListAppointments.addActionListener(this);
        appointmentsMenu.add(staffListAppointments);

        staffAddAvailableAppointmentDates = new JMenuItem("View/Add Available Appointment Days");
        staffAddAvailableAppointmentDates.addActionListener(this);
        appointmentsMenu.add(staffAddAvailableAppointmentDates);

        return menuBarGeneral;
    }

    private JMenuBar menuBarVisitor() {
        menuBarGeneral = new JMenuBar();

        appointmentsMenu = new JMenu("Appointments");
        menuBarGeneral.add(appointmentsMenu);

        visitorMakeAppointment = new JMenuItem("Make an appointment");
        visitorMakeAppointment.addActionListener(this);
        appointmentsMenu.add(visitorMakeAppointment);

        return menuBarGeneral;
    }

    private JMenuBar menuBarResearcher() {
        menuBarGeneral = new JMenuBar();

        appointmentsMenu = new JMenu("Appointments");
        menuBarGeneral.add(appointmentsMenu);

        staffListAppointments = new JMenuItem("List Appointments");
        staffListAppointments.addActionListener(this);
        appointmentsMenu.add(staffListAppointments);

        staffAddAvailableAppointmentDates = new JMenuItem("View/Add Available Appointment Days");
        staffAddAvailableAppointmentDates.addActionListener(this);
        appointmentsMenu.add(staffAddAvailableAppointmentDates);

        researcherMakeAppointment = new JMenuItem("Make an appointment");
        researcherMakeAppointment.addActionListener(this);
        appointmentsMenu.add(researcherMakeAppointment);

        return menuBarGeneral;
    }


    private JPanel adminPanel() {
        adminPanel = new JPanel(null);
        adminPanel.setVisible(false);
        adminPanel.setBounds(0, 0, getWidth(), getHeight()); // So as to fill the entire frame.

        JLabel adminWelcomeMsg = new JLabel("<html><h1 style=\"font-size:14px\">The University of Freeport's Admin Panel</h1></html>", JLabel.CENTER);
        adminWelcomeMsg.setBounds(0, ((getHeight() / 2) - 40), getWidth(), 40);
        adminPanel.add(adminWelcomeMsg);

        adminExitButton = new JButton("Exit");
        adminExitButton.addActionListener(this);
        adminExitButton.setBounds(148 + 90, 462, 90, 40);
        adminPanel.add(adminExitButton);

        return adminPanel;
    }

    private JPanel memberDetailsPanel() {
        memberDetailsPanel = new JPanel(null);
        memberDetailsPanel.setVisible(false);
        memberDetailsPanel.setBounds(getWidth() / 6, 0, getWidth(), getHeight()); // So as to fill the entire frame.

        JLabel typeLabel = new JLabel("<html><h1>Your Information</h1></html>", JLabel.CENTER);
        typeLabel.setBounds(90, 10, 200, 40);
        memberDetailsPanel.add(typeLabel);


        JLabel idLabel = new JLabel("ID");
        idLabel.setBounds(43, 50 + 30, 200, 30);
        memberDetailsPanel.add(idLabel);

        memberIdTxtFiled = new JTextField();
        memberIdTxtFiled.setBounds(40, 80 + 30, 130, 40);
//        memberIdTxtFiled.setBackground(new Color(214, 217, 223));
        memberIdTxtFiled.setEditable(false);
        memberDetailsPanel.add(memberIdTxtFiled);


        JLabel positionLabel = new JLabel("Position");
        positionLabel.setBounds(213, 50 + 30, 200, 30);
        memberDetailsPanel.add(positionLabel);

        positionTxtField = new JTextField();
        positionTxtField.setBounds(210, 80 + 30, 130, 40);
        positionTxtField.setEditable(false);
        memberDetailsPanel.add(positionTxtField);


        JLabel firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(43, 130 + 30, 200, 30);
        memberDetailsPanel.add(firstNameLabel);

        firstNameTxtField = new JTextField();
        firstNameTxtField.setBounds(40, 160 + 30, 130, 40);
        firstNameTxtField.setEditable(false);
        memberDetailsPanel.add(firstNameTxtField);


        JLabel lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(213, 130 + 30, 200, 30);
        memberDetailsPanel.add(lastNameLabel);

        lastNameTxtField = new JTextField();
        lastNameTxtField.setBounds(210, 160 + 30, 130, 40);
        lastNameTxtField.setEditable(false);
        memberDetailsPanel.add(lastNameTxtField);


        JLabel emailLabel = new JLabel("Email");
        emailLabel.setBounds(43, 210 + 30, 200, 30);
        memberDetailsPanel.add(emailLabel);

        emailTxtField = new JTextField();
        emailTxtField.setBounds(40, 240 + 30, 300, 40);
        emailTxtField.setEditable(false);
        memberDetailsPanel.add(emailTxtField);


        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(43, 300 + 30, 200, 30);
        memberDetailsPanel.add(addressLabel);

        addressTxtField = new JTextField();
        addressTxtField.setBounds(40, 330 + 30, 300, 40);
        addressTxtField.setEditable(false);
        memberDetailsPanel.add(addressTxtField);


        /*For testing*/
        /* typeLabel.setText("<html><h1>Student</h1></html>");
        memberIdTxtFiled.setText(university.getStudents().get("SHB1").getId());
        firstNameTxtField.setText(university.getStudents().get("SHB1").getFirstName());
        lastNameTxtField.setText(university.getStudents().get("SHB1").getLastName());
        emailTxtField.setText(university.getStudents().get("SHB1").getEmail());
        addressTxtField.setText(university.getStudents().get("SHB1").getAddress()); */

        exitBtn = new JButton("Exit");
        exitBtn.addActionListener(this);
        exitBtn.setBounds(148, 462, 90, 40);
        memberDetailsPanel.add(exitBtn);

        return memberDetailsPanel;
    }

    private JPanel visitorWelcomePanel() {
        visitorWelcomePanel = new JPanel(null);
        visitorWelcomePanel.setVisible(false);
        visitorWelcomePanel.setBounds(0, 0, getWidth(), getHeight()); // So as to fill the entire frame.

        JLabel visitorWelcome = new JLabel("<html><h2>Welcome to the University of Freeport</h1><br>" +
                "<p style=\"font-size=: 9px\">If you are a member of the university please exit and reenter program with your ID.</p></html>", JLabel.CENTER);
        visitorWelcome.setBounds(0, getHeight() / 2 - 50, getWidth(), 70);
        visitorWelcomePanel.add(visitorWelcome);

        exitVisitorBtn = new JButton("Exit");
        exitVisitorBtn.addActionListener(this);
        exitVisitorBtn.setBounds(248, 494, 90, 40);
        visitorWelcomePanel.add(exitVisitorBtn);

        return visitorWelcomePanel;
    }


    private JPanel adminRemoveMemberPanel() {
        adminRemoveMemberPanel = new JPanel(null);
        adminRemoveMemberPanel.setBounds(0, 0, getWidth(), getHeight());
        adminRemoveMemberPanel.setVisible(false);


        JScrollPane pane = new JScrollPane();
        pane.setBounds(0, 26, getWidth(), 423);


        adminRemoveMembersTable = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // prevents user editing table cell.
            }
        };

        adminRemoveMembersTable.setModel(new DefaultTableModel(
                new Object[][]{

                },

                new String[]{
                        "ID", "POSITION", "NAME"
                }
        ));

        adminRemoveMembersTable.setRowHeight(70);
        adminRemoveMembersTable.setBounds(0, 26, getWidth(), 423);

        adminRemoveMembersTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                adminRemoveMemberBtn.setEnabled(true);
            }
        });

        TableColumnModel columnModel = adminRemoveMembersTable.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(50);
        columnModel.getColumn(1).setPreferredWidth(150);
        columnModel.getColumn(2).setPreferredWidth(150 + 180);

        pane.setViewportView(adminRemoveMembersTable);


        closeAdminRemoveMemberPanelBtn = new JButton("Close");
        closeAdminRemoveMemberPanelBtn.addActionListener(this);
        closeAdminRemoveMemberPanelBtn.setBounds(248 - 95, 494, 90, 40);

        adminRemoveMemberBtn = new JButton("Remove");
        adminRemoveMemberBtn.addActionListener(this);
        adminRemoveMemberBtn.setEnabled(false);
        adminRemoveMemberBtn.setBounds(248+95 - 90, 494, 90, 40);

        adminRemoveMemberPanel.add(pane);
        adminRemoveMemberPanel.add(closeAdminRemoveMemberPanelBtn);
        adminRemoveMemberPanel.add(adminRemoveMemberBtn);


        return adminRemoveMemberPanel;
    }

    private JPanel memberReportPanel() {
        memberReportPanel = new JPanel(null);
        memberReportPanel.setBounds(0, 0, getWidth(), getHeight());
        memberReportPanel.setVisible(false);

        memberReportPane = new JTextPane();
        memberReportPane.setContentType("text/html");
        memberReportPane.setEditable(false);

        JScrollPane scroll = new JScrollPane(memberReportPane);
        scroll.setBounds(0, 0, getWidth(), 399);
        memberReportPanel.add(scroll);


        closeMemberReportPanelBtn = new JButton("Close");
        closeMemberReportPanelBtn.addActionListener(this);
        closeMemberReportPanelBtn.setBounds(265-90, 433 - 5, 90, 40);
        memberReportPanel.add(closeMemberReportPanelBtn);

        printMemberReportBtn = new JButton("Print");
        printMemberReportBtn.addActionListener(this);
        printMemberReportBtn.setBounds(265+5, 433 - 5, 90, 40);
        memberReportPanel.add(printMemberReportBtn);

        return memberReportPanel;
    }


    private JPanel searchedClassesPanel() {
        searchedClassesPanel = new JPanel(null);
        searchedClassesPanel.setBounds(0, 0, getWidth(), getHeight());
        searchedClassesPanel.setVisible(false);


        JScrollPane pane = new JScrollPane();
        pane.setBounds(0, 26, getWidth(), 423);


        searchedClassesTable = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // prevents user editing table cell.
            }
        };

        searchedClassesTable.setModel(new DefaultTableModel(
                new Object[][]{

                },

                new String[]{
                        "ID", "NAME", "TEACHER", "EXPERTISE"
                }
        ));

        searchedClassesTable.setRowHeight(70);
        searchedClassesTable.setBounds(0, 26, getWidth(), 423);

        TableColumnModel columnModel = searchedClassesTable.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(50);
        columnModel.getColumn(1).setPreferredWidth(150 + 180);
        columnModel.getColumn(2).setPreferredWidth(150);
        columnModel.getColumn(3).setPreferredWidth(120);

        searchedClassesTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {

                if (event.getClickCount() > 1) { // only run on a double click ()
                    tableMouseClicked();
                }
            }

        });

        pane.setViewportView(searchedClassesTable);


        JLabel searchedClassesPanelLabel = new JLabel("Double-Click to Select a course.", JLabel.CENTER);
        searchedClassesPanelLabel.setBounds(0, 0, getWidth(), 25);

        closeSearchedClassesPanel = new JButton("Close");
        closeSearchedClassesPanel.addActionListener(this);
        closeSearchedClassesPanel.setBounds(248, 494, 90, 40);


        searchedClassesPanel.add(searchedClassesPanelLabel);
        searchedClassesPanel.add(pane);
        searchedClassesPanel.add(closeSearchedClassesPanel);


        return searchedClassesPanel;
    }

    private JPanel classDetailsPanel() {
        classDetailsPanel = new JPanel(null);
        classDetailsPanel.setVisible(false);
//        classDetailsPanel.setBackground(Color.blue);
        classDetailsPanel.setBounds(getWidth() / 6, 0, 500, getHeight()); // So as to fill the entire frame.

        classNameLabel = new JLabel("", JLabel.CENTER);
        classNameLabel.setBounds(0, 0, classDetailsPanel.getWidth() - 114, 70);
//        classNameLabel.setBackground(Color.red);
        classDetailsPanel.add(classNameLabel);


        JLabel idLabel = new JLabel("ID");
        idLabel.setBounds(43, 50 + 30, 200, 30);
        classDetailsPanel.add(idLabel);

        classIdTxtField = new JTextField();
        classIdTxtField.setBounds(40, 80 + 30, 130, 40);
        classIdTxtField.setEditable(false);
        classDetailsPanel.add(classIdTxtField);


        JLabel categoryLabel = new JLabel("Category");
        categoryLabel.setBounds(213, 50 + 30, 200, 30);
        classDetailsPanel.add(categoryLabel);

        classCategoryTxtField = new JTextField();
        classCategoryTxtField.setBounds(210, 80 + 30, 130, 40);
        classCategoryTxtField.setEditable(false);
        classDetailsPanel.add(classCategoryTxtField);


        JLabel classTeacherName = new JLabel("Teacher");
        classTeacherName.setBounds(43, 130 + 30, 200, 30);
        classDetailsPanel.add(classTeacherName);

        classTeacherNameTxtField = new JTextField();
        classTeacherNameTxtField.setBounds(40, 160 + 30, 130, 40);
        classTeacherNameTxtField.setEditable(false);
        classDetailsPanel.add(classTeacherNameTxtField);


        JLabel availableSeatsLabel = new JLabel("Available Seats");
        availableSeatsLabel.setBounds(213, 130 + 30, 200, 30);
        classDetailsPanel.add(availableSeatsLabel);

        availableSeatsTxtField = new JTextField();
        availableSeatsTxtField.setBounds(210, 160 + 30, 130, 40);
        availableSeatsTxtField.setEditable(false);
        classDetailsPanel.add(availableSeatsTxtField);


        JLabel classRoomLabel = new JLabel("Room");
        classRoomLabel.setBounds(43, 210 + 30, 200, 30);
        classDetailsPanel.add(classRoomLabel);

        classRoomTxtField = new JTextField();
        classRoomTxtField.setBounds(40, 240 + 30, 130, 40);
        classRoomTxtField.setEditable(false);
        classDetailsPanel.add(classRoomTxtField);


        JLabel classDayLabel = new JLabel("Day");
        classDayLabel.setBounds(213, 210 + 30, 200, 30);
        classDetailsPanel.add(classDayLabel);

        classDayTxtField = new JTextField();
        classDayTxtField.setBounds(210, 240 + 30, 140, 40);
        classDayTxtField.setEditable(false);
        classDetailsPanel.add(classDayTxtField);


        JLabel classStartTimeLabel = new JLabel("Starts");
        classStartTimeLabel.setBounds(43, 210 + 30 + 80, 200, 30);
        classDetailsPanel.add(classStartTimeLabel);

        classStartsTxtField = new JTextField();
        classStartsTxtField.setBounds(40, 240 + 30 + 80, 140, 40);
        classStartsTxtField.setEditable(false);
        classDetailsPanel.add(classStartsTxtField);


        JLabel classEndTimeLabel = new JLabel("Ends");
        classEndTimeLabel.setBounds(213, 210 + 30 + 80, 200, 30);
        classDetailsPanel.add(classEndTimeLabel);

        classEndsTxtField = new JTextField();
        classEndsTxtField.setBounds(210, 240 + 30 + 80, 140, 40);
        classEndsTxtField.setEditable(false);
        classDetailsPanel.add(classEndsTxtField);


        registerBtn = new JButton("Register");
        registerBtn.addActionListener(this);
        registerBtn.setBounds(210 - 18, 445, 90, 40);
        classDetailsPanel.add(registerBtn);

        classDetailsBackBtn = new JButton("Back");
        classDetailsBackBtn.setBounds(115 - 18, 445, 90, 40);
        classDetailsBackBtn.addActionListener(this);
        classDetailsPanel.add(classDetailsBackBtn);

        return classDetailsPanel;
    }


    @SuppressWarnings("Duplicates")
    private JPanel listAppointmentsPanel() {
        listAppointmentsPanel = new JPanel(null);
        listAppointmentsPanel.setLayout(new BorderLayout());
        listAppointmentsPanel.setSize(getWidth(), getHeight());
        listAppointmentsPanel.setVisible(false);

        //Listing panel for the info loaded
        JPanel listingPanel = new JPanel();
        listingPanel.setLayout(null);
        listingPanel.setBounds(0, 0, getWidth(), getHeight());

        JLabel listAppointmentsLabel = new JLabel("<html><h1>Your Appointments</h1></html>", JLabel.CENTER);
        listAppointmentsLabel.setBounds(0, 0, getWidth(), 30);


        listAppointments_txtPane = new JTextPane();
        listAppointments_txtPane.setContentType("text/html");
        listAppointments_txtPane.setEditable(false);

        JScrollPane scroll = new JScrollPane(listAppointments_txtPane);
        scroll.setBounds(85, 75, 445, 300);

        listingPanel.add(listAppointmentsLabel);
        listingPanel.add(scroll);


        closeListAppointmentsPanelBtn = new JButton("Close");
        closeListAppointmentsPanelBtn.addActionListener(this);
//        closeListAppointmentsPanelBtn.setBounds(210-18 , 445, 90, 40);
        closeListAppointmentsPanelBtn.setBounds(265, 433 - 5, 90, 40);

        listingPanel.add(closeListAppointmentsPanelBtn);
        listAppointmentsPanel.add(listingPanel);
//        listAppointmentsPanel.add(closeListAppointmentsPanelBtn);

        return listAppointmentsPanel;
    }

    private JPanel addAvailableAppointmentDatesPanel() {
        addAvailableAppointmentDatesPanel = new JPanel(null);
        addAvailableAppointmentDatesPanel.setSize(getWidth(), getHeight());
        addAvailableAppointmentDatesPanel.setVisible(false);


        JLabel availableDatesLabel = new JLabel("<html><h1>Your Availability</h1></html>", JLabel.CENTER);
        availableDatesLabel.setBounds(0, 0, getWidth(), 30);
        addAvailableAppointmentDatesPanel.add(availableDatesLabel);


        listDates_txtPane = new JTextPane();
        listDates_txtPane.setContentType("text/html");
        listDates_txtPane.setEditable(false);

        JScrollPane scroll = new JScrollPane(listDates_txtPane);
        scroll.setBounds(135, 105 - 22, 335, 221);

        addAvailableAppointmentDatesPanel.add(scroll);


        //Add new available appointment date:


        JLabel addDateLbl = new JLabel("Add new Available Date", JLabel.CENTER);
        addDateLbl.setBounds(0, 373 - 22, getWidth(), 22);
        addAvailableAppointmentDatesPanel.add(addDateLbl);

        String[] days = {"Day", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        availableDatesDay = new JComboBox<>(days);
        availableDatesDay.setBounds(71, 405 - 22, 100, 24);
        addAvailableAppointmentDatesPanel.add(availableDatesDay);

        String[] dates = {"Date", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
        availableDatesDates = new JComboBox<>(dates);
        availableDatesDates.setBounds(181, 405 - 22, 100, 24);
        addAvailableAppointmentDatesPanel.add(availableDatesDates);

        String[] months = {"Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        availableDatesMonths = new JComboBox<>(months);
        availableDatesMonths.setBounds(291, 405 - 22, 100, 24);
        addAvailableAppointmentDatesPanel.add(availableDatesMonths);

        String[] time = {"Time", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00"};
        availableDatesTime = new JComboBox<>(time);
        availableDatesTime.setBounds(401, 405 - 22, 70, 24);
        addAvailableAppointmentDatesPanel.add(availableDatesTime);

        String[] period = {"AM", "PM"};
        availableDatesPeriod = new JComboBox<>(period);
        availableDatesPeriod.setBounds(481, 405 - 22, 51, 24);
        addAvailableAppointmentDatesPanel.add(availableDatesPeriod);


        addAvailableDateButton = new JButton("Add");
        addAvailableDateButton.setBounds(257, 439 - 22, 90, 31);
        addAvailableDateButton.addActionListener(this);
        addAvailableAppointmentDatesPanel.add(addAvailableDateButton);


        //the Close button:
        closeAddAvailableAppointmentDatesPanelBtn = new JButton("Close");
        closeAddAvailableAppointmentDatesPanelBtn.addActionListener(this);
        closeAddAvailableAppointmentDatesPanelBtn.setBounds(257, 488, 90, 40);
        addAvailableAppointmentDatesPanel.add(closeAddAvailableAppointmentDatesPanelBtn);

        return addAvailableAppointmentDatesPanel;
    }

    private JPanel makeAppointmentPanel() {
        makeAppointmentPanel = new JPanel(null);
        makeAppointmentPanel.setBounds(0, 0, getWidth(), getHeight());
        makeAppointmentPanel.setVisible(false);

        JLabel makeAppointmentLabel = new JLabel("<html><h1>Create an Appointment</h1>\n" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"color:#a60000\">Please fill out all fields</span></html>", JLabel.CENTER);
        makeAppointmentLabel.setBounds(0, 0, getWidth(), 75);
        makeAppointmentPanel.add(makeAppointmentLabel);


        JLabel firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(43 + 115, 50 + 30, 200, 30);
        makeAppointmentPanel.add(firstNameLabel);

        makeAppointmentFirstNameField = new JTextField();
        makeAppointmentFirstNameField.setBounds(40 + 115, 80 + 30, 130, 40);
        makeAppointmentFirstNameField.setEditable(false);
        makeAppointmentPanel.add(makeAppointmentFirstNameField);


        JLabel lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(213 + 115, 50 + 30, 200, 30);
        makeAppointmentPanel.add(lastNameLabel);

        makeAppointmentLastNameField = new JTextField();
        makeAppointmentLastNameField.setBounds(210 + 115, 80 + 30, 130, 40);
        makeAppointmentLastNameField.setEditable(false);
        makeAppointmentPanel.add(makeAppointmentLastNameField);


        JLabel emailLabel = new JLabel("Email");
        emailLabel.setBounds(43 + 115, 130 + 30, 200, 30);
        makeAppointmentPanel.add(emailLabel);

        makeAppointmentEmailField = new JTextField();
        makeAppointmentEmailField.setBounds(40 + 115, 160 + 30, 300, 40);
        makeAppointmentEmailField.setEditable(false);
        makeAppointmentPanel.add(makeAppointmentEmailField);


        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(43 + 115, 210 + 30, 200, 30);
        makeAppointmentPanel.add(addressLabel);

        makeAppointmentAddressField = new JTextField();
        makeAppointmentAddressField.setBounds(40 + 115, 240 + 30, 300, 40);
        makeAppointmentAddressField.setEditable(false);
        makeAppointmentPanel.add(makeAppointmentAddressField);


        JLabel chooseStaffLabel = new JLabel("Choose the Staff Member you to see: ");
        chooseStaffLabel.setBounds(43 + 115, 300 + 30, 280, 30);
        makeAppointmentPanel.add(chooseStaffLabel);

        chooseStaffDropdown = new JComboBox<>();
        chooseStaffDropdown.setBounds(40 + 115, 330 + 30, 300, 40);
        makeAppointmentPanel.add(chooseStaffDropdown);


        researcherCloseMakeAppointmentPanelBtn = new JButton("Back");
        researcherCloseMakeAppointmentPanelBtn.setBounds(115 - 18 + 115 - 27, 445, 90, 40);
        researcherCloseMakeAppointmentPanelBtn.addActionListener(this);
        makeAppointmentPanel.add(researcherCloseMakeAppointmentPanelBtn);

        visitorCloseMakeAppointmentPanelBtn = new JButton("Back");
        visitorCloseMakeAppointmentPanelBtn.setBounds(115 - 18 + 115 - 27, 445, 90, 40);
        visitorCloseMakeAppointmentPanelBtn.addActionListener(this);
        makeAppointmentPanel.add(visitorCloseMakeAppointmentPanelBtn);

        researcherMakeAppointmentBtn = new JButton("Make Appointment");
        researcherMakeAppointmentBtn.setBounds(210 - 18 + 115 - 27, 445, 135, 40);
        researcherMakeAppointmentBtn.addActionListener(this);
        researcherMakeAppointmentBtn.setVisible(false);
        makeAppointmentPanel.add(researcherMakeAppointmentBtn);

        visitorMakeAppointmentBtn = new JButton("Make Appointment");
        visitorMakeAppointmentBtn.setBounds(210 - 18 + 115 - 27, 445, 135, 40);
        visitorMakeAppointmentBtn.addActionListener(this);
        visitorMakeAppointmentBtn.setVisible(false);
        makeAppointmentPanel.add(visitorMakeAppointmentBtn);

        return makeAppointmentPanel;
    }


    @SuppressWarnings("Duplicates")
    @Override
    public void actionPerformed(ActionEvent e) {
        //region Login actions
        if (e.getSource() == studentBtn) {
            login("Student");
        }
        if (e.getSource() == teacherBtn) {
            login("Teacher");

        }
        if (e.getSource() == researcherBtn) {
            login("Researcher");

        }
        if (e.getSource() == visitorBtn) {
            loginPanel.setVisible(false);

            visitorWelcomePanel.setVisible(true);
            this.setJMenuBar(menuBarVisitor());
        }
        if (e.getSource() == adminBtn) {
            String pswd = JOptionPane.showInputDialog(null, "Please Enter Administrator Password.");

            if (pswd == null) {
                JOptionPane.getRootFrame().dispose(); // destroy pane if is clicked `x`/Cancel.

            } else if (!pswd.equals(university.getAdminPassword())) { // If the incorrect password was given.
                JOptionPane.showMessageDialog(null, "Incorrect Admin password.", "Error", JOptionPane.ERROR_MESSAGE);
                actionPerformed(e);

            } else { // The password is correct
                loginPanel.setVisible(false);

                adminPanel.setVisible(true);

                this.setJMenuBar(menuBarAdmin());
            }
        }
        //endregion


        //region admin Menu actions
        if (e.getSource() == adminStaffRemove) {
            ArrayList<UniversityMember> staffRemoveMemberList = new ArrayList<>(university.getStaff().values());

            if (!staffRemoveMemberList.isEmpty()) {
                adminRemoveMembersModel = (DefaultTableModel) adminRemoveMembersTable.getModel();

                Object rowData[] = new Object[4];

                for (UniversityMember staffMember : staffRemoveMemberList) {
                    rowData[0] = staffMember.getId();
                    if (staffMember instanceof Researcher) {
                        rowData[1] = "Researcher";
                    } else if (staffMember instanceof Teacher) {
                        rowData[1] = "Teacher";
                    } else rowData[1] = "";
                    rowData[2] = staffMember.getFirstName() + " " + staffMember.getLastName();

                    adminRemoveMembersModel.addRow(rowData);
                }

                adminPanel.setVisible(false);
                adminMenuBar.setEnabled(false);
                adminStaffMenu.setEnabled(false);
                adminStudentMenu.setEnabled(false);

                adminRemoveMemberPanel.setVisible(true);


            } else {
                JOptionPane.showMessageDialog(null, "There are currently no Staff members!",
                        "University of Freeport", JOptionPane.WARNING_MESSAGE);
            }
        }

        if (e.getSource() == adminStudentRemove) {
            ArrayList<UniversityMember> studentRemoveMemberList = new ArrayList<>(university.getStudents().values());

            if (!studentRemoveMemberList.isEmpty()) {
                adminRemoveMembersModel = (DefaultTableModel) adminRemoveMembersTable.getModel();

                Object rowData[] = new Object[4];

                for (UniversityMember studentMember : studentRemoveMemberList) {
                    rowData[0] = studentMember.getId();
                    rowData[1] = "Student";
                    rowData[2] = studentMember.getFirstName() + " " + studentMember.getLastName();

                    adminRemoveMembersModel.addRow(rowData);
                }

                adminPanel.setVisible(false);
                adminMenuBar.setEnabled(false);
                adminStaffMenu.setEnabled(false);
                adminStudentMenu.setEnabled(false);

                adminRemoveMemberPanel.setVisible(true);


            } else {
                JOptionPane.showMessageDialog(null, "There are currently no Student members!",
                        "University of Freeport", JOptionPane.WARNING_MESSAGE);
            }
        }

        if (e.getSource() == adminStaffReport) {
            StringBuilder staffReport = new StringBuilder();
            for (UniversityMember staffMember : university.getStaff().values()) {

                StringBuilder classes = new StringBuilder();
                for (Course course : university.getCourses().values()) {
                    if (course.getTeacher().getId().equals(staffMember.getId())) {
                        classes.append("     ").append(course.getName()).append("<br>");
                    }
                }

                String staffMemberReport =
                        "<br>" + "_______________________________________________________" +
                        "<br><b>ID: </b>" + staffMember.getId() +
                        "<br><b>Position: </b>" + (staffMember instanceof Researcher ? "Researcher" : ((Teacher)staffMember).getTitle() ) +
                        "<br><b>Name: </b>" + staffMember.getFirstName() + " " + staffMember.getLastName() +
                        (staffMember instanceof Teacher ? "<br><b>Classes: </b>" + "<br>" + classes : "") +
                        "<br><b>Appointments: </b>" + "<br>" + university.listAppointments(staffMember.getId()) +
                                "_______________________________________________________" +
                        "<br>"+
                        "<br>";


                staffReport.append(staffMemberReport);
            }

            memberReportPane.setText(String.valueOf(staffReport));

            adminPanel.setVisible(false);
            adminMenuBar.setEnabled(false);
            adminStaffMenu.setEnabled(false);
            adminStudentMenu.setEnabled(false);

            memberReportPanel.setVisible(true);
        }

        if (e.getSource() == adminStudentReport) {
            StringBuilder studentReport = new StringBuilder();
            for (UniversityMember studentMember : university.getStudents().values()) {

                StringBuilder classes = new StringBuilder();
                for (StudentSchedule schedule : university.getStudentSchedules()) {

                    if (schedule.getStudent().getId().equals(studentMember.getId())) {

                        for (Course course : university.getCourses().values()) {

                            if (course.getCourseId().equals(schedule.getCourseId())) {
                                classes.append(course.getName()).append("<br>");
                            }
                        }
                    }
                }

                String studentMemberReport =
                        "<br>" + "_______________________________________________________" +
                                "<br><b>ID: </b>" + studentMember.getId() +
                                "<br><b>Position: </b>" + "Student" +
                                "<br><b>Name: </b>" + studentMember.getFirstName() + " " + studentMember.getLastName() +
                                "<br><b>Classes: </b>" + "<br>" + classes +
                                "_______________________________________________________" +
                                "<br>"+
                                "<br>";


                studentReport.append(studentMemberReport);
            }

            memberReportPane.setText(String.valueOf(studentReport));

            adminPanel.setVisible(false);
            adminMenuBar.setEnabled(false);
            adminStaffMenu.setEnabled(false);
            adminStudentMenu.setEnabled(false);

            memberReportPanel.setVisible(true);
        }
        //endregion

        //region Student Menu actions
        if (e.getSource() == searchClassesByName) {
            String courseName = JOptionPane.showInputDialog(null, "Enter Course Name to Search for",
                    "Search Course By Name", JOptionPane.QUESTION_MESSAGE);

            if (courseName == null) {
                JOptionPane.getRootFrame().dispose();

            } else if (courseName.equals("")) {
                JOptionPane.showMessageDialog(null, "Please enter a value to search by!",
                        "No Value Entered", JOptionPane.ERROR_MESSAGE);
                actionPerformed(e);

            } else {
                showClassSearchPanel("name", courseName, e);
            }
        }

        if (e.getSource() == programming) {
            showClassSearchPanel("expertise", "programming", e);
        }

        if (e.getSource() == physics) {
            showClassSearchPanel("expertise", "physics", e);
        }

        if (e.getSource() == dataAnalysis) {
            showClassSearchPanel("expertise", "Data Analysis", e);
        }

        if (e.getSource() == math) {
            showClassSearchPanel("expertise", "Mathematics", e);
        }

        if (e.getSource() == searchClassesByTeacher) {
            String teacherName = JOptionPane.showInputDialog(null, "Enter Teacher Name to Search for",
                    "Search Course By Teacher", JOptionPane.QUESTION_MESSAGE);

            if (teacherName == null) {
                JOptionPane.getRootFrame().dispose();

            } else if (teacherName.equals("")) {
                JOptionPane.showMessageDialog(null, "Please enter a value to search by!",
                        "No Value Entered", JOptionPane.ERROR_MESSAGE);
                actionPerformed(e);

            } else {
                showClassSearchPanel("Teacher", teacherName, e);
            }
        }
        //endregion

        //region Researcher Menu actions
        if (e.getSource() == researcherMakeAppointment) {
            memberDetailsPanel.setVisible(false);
            menuBarGeneral.setEnabled(false);
            appointmentsMenu.setEnabled(false);

            makeAppointmentFirstNameField.setText(firstNameTxtField.getText());
            makeAppointmentFirstNameField.setEditable(false);

            makeAppointmentLastNameField.setText(lastNameTxtField.getText());
            makeAppointmentLastNameField.setEditable(false);

            makeAppointmentEmailField.setText(emailTxtField.getText());
            makeAppointmentEmailField.setEditable(false);

            makeAppointmentAddressField.setText(addressTxtField.getText());
            makeAppointmentAddressField.setEditable(false);


            for (UniversityMember staff : university.getStaff().values()) { //Just get the teachers.
                if (staff instanceof Teacher) {
                    chooseStaffDropdown.addItem(staff.getFirstName() + " " + staff.getLastName());
                }
            }

            researcherMakeAppointmentBtn.setVisible(true);
            visitorMakeAppointmentBtn.setVisible(false); // Just to be sure
            researcherCloseMakeAppointmentPanelBtn.setVisible(true);
            visitorCloseMakeAppointmentPanelBtn.setVisible(false);

            makeAppointmentPanel.setVisible(true);
        }
        //endregion

        //region Teacher Menu Actions.
        if (e.getSource() == staffListAppointments) {
            listAppointments_txtPane.setText(university.listAppointments(memberIdTxtFiled.getText()));
            listAppointmentsPanel.setVisible(true);

            memberDetailsPanel.setVisible(false);
            menuBarGeneral.setEnabled(false);
            appointmentsMenu.setEnabled(false);
        }

        if (e.getSource() == staffAddAvailableAppointmentDates) {
            StringBuilder availableDates = new StringBuilder();
            for (Date availableDate : university.getAvailableAppointmentDates().get(memberIdTxtFiled.getText())) {
                availableDates.append(formatAppointmentDate.format(availableDate)).append("<br><br>");
            }

            addAvailableAppointmentDatesPanel.setVisible(true);
            listDates_txtPane.setText(availableDates.toString());

            memberDetailsPanel.setVisible(false);
            menuBarGeneral.setEnabled(false);
            appointmentsMenu.setEnabled(false);

        }
        //endregion

        //region Visitor menu actions
        if (e.getSource() == visitorMakeAppointment) {
            visitorWelcomePanel.setVisible(false);
            menuBarGeneral.setEnabled(false);
            appointmentsMenu.setEnabled(false);

            makeAppointmentFirstNameField.setEditable(true);
            makeAppointmentLastNameField.setEditable(true);
            makeAppointmentEmailField.setEditable(true);
            makeAppointmentAddressField.setEditable(true);

            for (UniversityMember staff : university.getStaff().values()) { //Get all the staff (teachers & researchers).
                chooseStaffDropdown.addItem(staff.getFirstName() + " " + staff.getLastName());
            }

            visitorMakeAppointmentBtn.setVisible(true);
            researcherMakeAppointmentBtn.setVisible(false);  // Just to be sure
            researcherCloseMakeAppointmentPanelBtn.setVisible(false);
            visitorCloseMakeAppointmentPanelBtn.setVisible(true);

            makeAppointmentPanel.setVisible(true);
        }
        //endregion


        //region admin remove member actions
        if (e.getSource() == adminRemoveMemberBtn) {
            int row = adminRemoveMembersTable.getSelectedRow();

            TableModel model = adminRemoveMembersTable.getModel();

            String memberPosition = (String) model.getValueAt(row, 1);
            String memberName = (String) model.getValueAt(row, 2);

            if (university.removeMember((String) model.getValueAt(row, 0))) {
                JOptionPane.showMessageDialog(null, "You have successfully the " + memberPosition + " " + memberName);
                closeAdminRemoveMemberPanelBtn.doClick();
            } else {
                JOptionPane.showMessageDialog(null, "There was an error preforming the operation!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource() == closeAdminRemoveMemberPanelBtn) {
            adminRemoveMembersModel.setRowCount(0);
            adminRemoveMemberPanel.setVisible(false);

            adminMenuBar.setEnabled(true);
            adminStaffMenu.setEnabled(true);
            adminStudentMenu.setEnabled(true);

            adminPanel.setVisible(true);
        }
        //endregion

        //region admin Report actions
        if (e.getSource() == closeMemberReportPanelBtn) {
            memberReportPane.setText(null);
            memberReportPanel.setVisible(false);

            adminMenuBar.setEnabled(true);
            adminStaffMenu.setEnabled(true);
            adminStudentMenu.setEnabled(true);

            adminPanel.setVisible(true);

        }

        if (e.getSource() == printMemberReportBtn) {
            try {
                boolean done = memberReportPane.print();
                if (done) {
                    JOptionPane.showMessageDialog(null, "Printing is done");
                } else {
                    JOptionPane.showMessageDialog(null, "Print was canceled");
                }
            } catch (Exception pex) {
                JOptionPane.showMessageDialog(null, "Error while printing");
                pex.printStackTrace();
            }
        }
        //endregion

        //region List Appointments Panel actions
        if (e.getSource() == closeListAppointmentsPanelBtn) {
            listAppointments_txtPane.setText(null);
            listAppointmentsPanel.setVisible(false);

            memberDetailsPanel.setVisible(true);
            menuBarGeneral.setEnabled(true);
            appointmentsMenu.setEnabled(true);
        }
        //endregion

        //region Add available appoint actions
        if (e.getSource() == closeAddAvailableAppointmentDatesPanelBtn) {
            addAvailableAppointmentDatesPanel.setVisible(false);
            listDates_txtPane.setText(null);
            availableDatesDay.setSelectedIndex(0);
            availableDatesDates.setSelectedIndex(0);
            availableDatesMonths.setSelectedIndex(0);
            availableDatesTime.setSelectedIndex(0);


            memberDetailsPanel.setVisible(true);
            menuBarGeneral.setEnabled(true);
            appointmentsMenu.setEnabled(true);
        }

        if (e.getSource() == addAvailableDateButton) {
            if (availableDatesDay.getSelectedIndex() == 0 || (availableDatesDates.getSelectedIndex() == 0) ||
                    (availableDatesMonths.getSelectedIndex() == 0) || availableDatesTime.getSelectedIndex() == 0) {
                JOptionPane.showMessageDialog(null, "Please select a value for ALL fields!", "Error", JOptionPane.WARNING_MESSAGE);
            } else {
                String date = Objects.requireNonNull(availableDatesDay.getSelectedItem()).toString() + " " + Objects.requireNonNull(availableDatesDates.getSelectedItem()).toString() + " "
                        + Objects.requireNonNull(availableDatesMonths.getSelectedItem()).toString() + " " + Objects.requireNonNull(availableDatesTime.getSelectedItem()).toString() + " " + Objects.requireNonNull(availableDatesPeriod.getSelectedItem()).toString();
                try {
                    university.addAvailableAppointmentDate(memberIdTxtFiled.getText(), formatAppointmentDate.parse(date));
                    JOptionPane.showMessageDialog(null, "Successfully added Date!");
                    staffAddAvailableAppointmentDates.doClick(); // I currently know no better way to refresh the data in the txt pane.
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
        }

        //endregion

        //region Searched Classes Panel(with the table) Actions
        if (e.getSource() == closeSearchedClassesPanel) {
            searchedClassesModel.setRowCount(0);
            searchedClassesPanel.setVisible(false);

            menuBarStudent.setEnabled(true);
            search.setEnabled(true);

            memberDetailsPanel.setVisible(true);
        }
        //endregion

        //region Course Details Actions
        if (e.getSource() == classDetailsBackBtn) {
            classNameLabel.setText(null);
            classIdTxtField.setText(null);
            classTeacherNameTxtField.setText(null);
            classCategoryTxtField.setText(null);
            availableSeatsTxtField.setText(null);
            classRoomTxtField.setText(null);
            classDayTxtField.setText(null);
            classStartsTxtField.setText(null);
            classEndsTxtField.setText(null);

            classDetailsPanel.setVisible(false);
            searchedClassesPanel.setVisible(true);
        }

        if (e.getSource() == registerBtn) {
            String[] results = university.registerStudent(classIdTxtField.getText(), university.getStudentById(memberIdTxtFiled.getText()));
            if (results[0].equalsIgnoreCase("true")) {  // If registration was successful:
                JOptionPane.showMessageDialog(null, results[1],
                        "Successfully registered!", JOptionPane.INFORMATION_MESSAGE);

                classDetailsBackBtn.doClick();
            } else {   // If registration was unsuccessful:
                JOptionPane.showMessageDialog(null, results[1],  // then show error msg.
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        //endregion

        //region Make Appointment Actions
        if (e.getSource() == researcherCloseMakeAppointmentPanelBtn) {
            makeAppointmentFirstNameField.setText(null);
            makeAppointmentLastNameField.setText(null);
            makeAppointmentEmailField.setText(null);
            makeAppointmentAddressField.setText(null);
            chooseStaffDropdown.removeAllItems();
            makeAppointmentPanel.setVisible(false);

            memberDetailsPanel.setVisible(true);
            menuBarGeneral.setEnabled(true);
            appointmentsMenu.setEnabled(true);
        }

        if (e.getSource() == researcherMakeAppointmentBtn) {
            makeAppointment("researcher");
        }

        if (e.getSource() == visitorMakeAppointmentBtn) {
            //Make sure all data is entered
            if ((makeAppointmentFirstNameField.getText().matches(" *") || makeAppointmentFirstNameField.getText().isEmpty()) ||
                    (makeAppointmentLastNameField.getText().matches(" *") || makeAppointmentLastNameField.getText().isEmpty()) ||
                    (makeAppointmentEmailField.getText().matches(" *") || makeAppointmentEmailField.getText().isEmpty()) ||
                    (makeAppointmentAddressField.getText().matches(" *") || makeAppointmentAddressField.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Please make sure to fill out ALL fields!", "Error", JOptionPane.ERROR_MESSAGE);

            } else makeAppointment("visitor");
        }

        if (e.getSource() == visitorCloseMakeAppointmentPanelBtn) {
            makeAppointmentFirstNameField.setText(null);
            makeAppointmentLastNameField.setText(null);
            makeAppointmentEmailField.setText(null);
            makeAppointmentAddressField.setText(null);
            chooseStaffDropdown.removeAllItems();
            makeAppointmentPanel.setVisible(false);

            visitorWelcomePanel.setVisible(true);
            menuBarGeneral.setEnabled(true);
            appointmentsMenu.setEnabled(true);
        }
        //endregion


        //region Exit Application action(s)
        if (e.getSource() == exitBtn) {
            this.setJMenuBar(null);
            memberIdTxtFiled.setText(null);
            firstNameTxtField.setText(null);
            lastNameTxtField.setText(null);
            emailTxtField.setText(null);
            addressTxtField.setText(null);
            positionTxtField.setText(null);

            memberDetailsPanel.setVisible(false);
            loginPanel.setVisible(true);
        }
        //region Exit visitor welcome panel action
        if (e.getSource() == exitVisitorBtn) {
            this.setJMenuBar(null);
            visitorWelcomePanel.setVisible(false);
            loginPanel.setVisible(true);
        }
        //endregion
        //region Exit Admin Welcome Panel.
        if (e.getSource() == adminExitButton) {
            this.setJMenuBar(null);
            adminPanel.setVisible(false);
            loginPanel.setVisible(true);
        }
        //endregion
        //endregion


    }



    private void makeAppointment(String type) {
        User user;
        ArrayList<String> availableDates = new ArrayList<>();

        for (Date date : university.getAvailableAppointmentDates().get(university.getStaffByName(Objects.requireNonNull(chooseStaffDropdown.getSelectedItem()).toString()).getId())) {
            availableDates.add(formatAppointmentDate.format(date));
        }


        String dateString = (String) JOptionPane.showInputDialog(null, "Select a date from the list below:",
                "Choose date to see staff member...", JOptionPane.QUESTION_MESSAGE, null, availableDates.toArray(), "Select a Date");

        if (dateString != null) {
            if (type.equalsIgnoreCase("Researcher")) {
                user = university.getResearcherById(memberIdTxtFiled.getText());
            } else {
                user = new Visitor(makeAppointmentFirstNameField.getText(), makeAppointmentLastNameField.getText(), makeAppointmentAddressField.getText(),
                        makeAppointmentEmailField.getText());
            }
            try {
                String[] result = university.addAppointment(user,
                        university.getStaffByName(Objects.requireNonNull(chooseStaffDropdown.getSelectedItem()).toString()),
                        formatAppointmentDate.parse(dateString));
                if (result[0].equalsIgnoreCase("true")) {
                    JTextPane successPane = new JTextPane();
                    successPane.setContentType("text/html");
                    successPane.setText("You have successfully created an Appointment:" + university.getAppointments().get(result[1]).toString());
                    JOptionPane.showMessageDialog(null, successPane);

                    //Delete date:
                    university.getAvailableAppointmentDates().get(university.getTeacherByName(Objects.requireNonNull(chooseStaffDropdown.getSelectedItem()).toString()).getId()).remove(formatAppointmentDate.parse(dateString));
                }
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        } else {
            JOptionPane.getRootFrame().dispose();
        }

    }

    private void showClassSearchPanel(String criteria, String value, ActionEvent e) {

        ArrayList<Course> searchedCourses = university.searchClasses(criteria, value);

        if (searchedCourses.isEmpty()) {
            if (criteria.equalsIgnoreCase("expertise")) {
                JOptionPane.showMessageDialog(null, "There are no available courses in this category yet.",
                        "Error", JOptionPane.ERROR_MESSAGE);
            } else if (criteria.equalsIgnoreCase("Name")) {
                JOptionPane.showMessageDialog(null, "No courses of that name exists",
                        "Error", JOptionPane.ERROR_MESSAGE);
                actionPerformed(e);
            } else if (criteria.equalsIgnoreCase("Teacher")) {
                JOptionPane.showMessageDialog(null, "There are currently no available courses taught by " + value,
                        "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            searchedClassesModel = (DefaultTableModel) searchedClassesTable.getModel();

            Object rowData[] = new Object[4];

            for (Course course : searchedCourses) {
                rowData[0] = course.getCourseId();
                rowData[1] = course.getName();
                rowData[2] = course.getTeacher().getFirstName() + " " + course.getTeacher().getLastName();
                rowData[3] = course.getSubject();

                searchedClassesModel.addRow(rowData);
            }

            memberDetailsPanel.setVisible(false);
            menuBarStudent.setEnabled(false);
            search.setEnabled(false);
            searchedClassesPanel.setVisible(true);
        }

    }

    private void tableMouseClicked() {
        //Not clearing table values to enable returning to it.
        SimpleDateFormat formatDay = new SimpleDateFormat("EEEE");
        SimpleDateFormat formatStartTime = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat formatEndTime = new SimpleDateFormat("hh:mm a");

        int row = searchedClassesTable.getSelectedRow();

        TableModel tableModel = searchedClassesTable.getModel();

        String courseId = tableModel.getValueAt(row, 0).toString();

        Course course = university.getCourseById(courseId);


        if (course != null) {
            searchedClassesPanel.setVisible(false);

            classNameLabel.setText("<html><b>" + course.getName() + "</b></html>");
            classIdTxtField.setText(course.getCourseId());
            classTeacherNameTxtField.setText(course.getTeacher().getFirstName() + " " + course.getTeacher().getLastName());
            classCategoryTxtField.setText(course.getSubject());
            availableSeatsTxtField.setText(Integer.toString((course.getClassroom().getMaxSeats() - course.getClassroom().getOccupiedSeats())));
            classRoomTxtField.setText(course.getClassroom().getName());
            classDayTxtField.setText(formatDay.format(course.getStartTime()));
            classStartsTxtField.setText(formatStartTime.format(course.getStartTime()));
            classEndsTxtField.setText(formatEndTime.format(course.getEndTime()));
            classDetailsPanel.setVisible(true);
        } else {
            System.out.println("Course not exist!");
            System.out.println(row);
            System.out.println(courseId);
        }
    }

    private void login(String type) {
        String id = JOptionPane.showInputDialog(null, "Please Enter ID");
        UniversityMember member = null;
        if (type.equalsIgnoreCase("Student")) {
            member = university.checkExistId("Student", id);
            positionTxtField.setText("Student");
            if (member != null) {
                this.setJMenuBar(menuBarStudent());
//                this.add(menuBarStudent());
            }
        } else if (type.equalsIgnoreCase("Teacher")) {
            member = university.checkExistId("Teacher", id);
            if (member != null) {
                positionTxtField.setText(((Teacher) member).getTitle());  // Type casting was the only way this worked.
                this.setJMenuBar(menuBarStaff());
            }
        } else if (type.equalsIgnoreCase("Researcher")) {
            member = university.checkExistId("Researcher", id);
            if (member != null) {
                positionTxtField.setText("Researcher");
                this.setJMenuBar(menuBarResearcher());
            }
        }
        if (id == null) {
            JOptionPane.getRootFrame().dispose(); // destroy pane if is clicked `x`/Cancel.
        } else if (member == null) { // I.E. If member does not exist:
            JOptionPane.showMessageDialog(null, "ID Does Not Exist", "Error", JOptionPane.ERROR_MESSAGE);
            login(type); // Call function so as to trigger the enter ID pane to open again.

        } else { //If Cancel was not pressed AND id exists:
            loginPanel.setVisible(false);
            memberDetailsPanel.setVisible(true);


            memberIdTxtFiled.setText(member.getId());
            firstNameTxtField.setText(member.getFirstName());
            lastNameTxtField.setText(member.getLastName());
            emailTxtField.setText(member.getEmail());
            addressTxtField.setText(member.getAddress());
        }
    }
}
