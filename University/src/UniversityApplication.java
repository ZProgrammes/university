import javax.swing.*;
import java.text.ParseException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

//import java.text.SimpleDateFormat;

public class UniversityApplication {

    //Main function that will initialise the GUI.
    public static void main(String[] args) {

        SwingWorker<Boolean, Void> worker;
        worker = new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() throws ParseException {
                Data data = new Data();
                data.loadData();
                return true;
            }

            //Run the GUI

            @Override
            protected void done() {
                try {
                    //was `status = get()` but intellj suggested otherwise and so it became:
                    get();
                    try {
                        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                        UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                    } catch (IllegalAccessException | ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException e) {
                        e.printStackTrace();
                        System.out.println("\nThe set Look and Feel is giving trouble:\n\tSuggest commenting out if problem persists");
                    }

                    new GUI2();

                } catch (InterruptedException | ExecutionException e) {
                    Logger.getLogger(UniversityApplication.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        };

        //Run the application
        //FIXME: If program not working it might be because of this being    worker.run();   instead of   worker.execute();
        worker.run();
//        Data data = new Data(); data.loadData();
//
//        SimpleDateFormat df = new SimpleDateFormat("EEEE hh:mm a");
//
//
//        University university = University.getInstance();
//        Course aclass = new Course("P435", university.getTeacherById("THD1"), university.getClassroomByName("B12"), "Programming",
//                "Programming and Software Engineering Practices", df.parse("Wednesday 12:30 AM"), df.parse("Wednesday 02:00 PM"));
//
//        System.out.println("Course 1: ");
//        System.out.println(aclass.getStartTime() + "\t" + df.format(aclass.getStartTime()));
//        System.out.println(aclass.getEndTime() + "\t" + df.format(aclass.getEndTime()));
//
//        System.out.println();
//
//        Course a2ndclass = new Course("P437", university.getTeacherById("THD1"), university.getClassroomByName("B12"), "Programming",
//                "Programming and Software Engineering Practices", df.parse("Wednesday 1:30 AM"), df.parse("Wednesday 03:00 PM"));
//
//        System.out.println("Course 2: ");
//        System.out.println(a2ndclass.getStartTime() + "\t" + df.format(a2ndclass.getStartTime()));
//        System.out.println(a2ndclass.getEndTime() + "\t" + df.format(a2ndclass.getEndTime()));


//        checkClash(a2ndclass, aclass);


//        System.out.println(df.format(a2ndclass.getStartTime()));
//    /**
//     * date1.compareTo(date2) > 0 : "Date1 is after Date2"
//     * date1.compareTo(date2) < 0 : "Date1 is before Date2"
//     * date1.compareTo(date2) == 0: "Date1 is equal to Date2"
//     * */
//    public static void checkClash(Course class1, Course class2){
//        if (class2.getStartTime().before(class1.getStartTime()) && (class2.getEndTime().after(class1.getStartTime()) ))
//            System.out.println("Class2 clashes with Class1 : End in between");
//        else if (class2.getStartTime().after(class1.getStartTime()) && (class2.getStartTime().before(class1.getEndTime()))) {
//            System.out.println("Class2 clashes with Class1 : Start in between");
//        } else if (class2.getEndTime().equals(class1.getStartTime()) || (class2.getEndTime().equals(class1.getEndTime()))) {
//            System.out.println("Class2 clashes with Class1 : End equals");
//        } else if (class2.getStartTime().equals(class1.getEndTime()) || (class2.getStartTime().equals(class1.getStartTime()))) {
//            System.out.println("Class2 clashes with Class1 : Start equals");
//        }
//
//    }

    }
}
