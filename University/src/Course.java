import java.util.ArrayList;
import java.util.Date;

public class Course {
    //Will have a Classroom, a Teacher, Students and other related fields.
    private Teacher teacher;
    private Classroom classroom;
    private String subject, name;
    private Date startTime;
    private Date endTime;
    private String courseId;

    public Course(String courseId, Teacher teacher, Classroom classroom, String subject, String name, Date startTime, Date endTime) {
        this.teacher = teacher;
        this.classroom = classroom;
        this.subject = subject;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.courseId = courseId;

        classroom.setStatus("occupied");
    }


    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
}