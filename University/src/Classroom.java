public class Classroom {
    // Object is an individual room of the University
    // with fields such as maxSeats

    private String status = "unoccupied"; // occupied | unoccupied | maintenance | ect...
    private int  maxSeats, occupiedSeats;
    private String name;

    public Classroom(String name, int maxSeats) {
        this.name = name;
        this.maxSeats = maxSeats;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupiedSeats() {
        return occupiedSeats;
    }

    public void setOccupiedSeats() {
        this.occupiedSeats += 1;
    }
}