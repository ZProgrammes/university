import java.util.ArrayList;
import java.util.Date;

public interface UniversityInterface {

//    void addTeacher(String title, String firstName, String lastName);

    void addTeacher(String title, String firstName, String lastName, int officeNumber, String address, String email);

//    void addStudent(String firstName, String lastName);

    void addStudent(String firstName, String lastName, String address, String email);

    void addResearcher(String firstName, String lastName, int officeNumber, String address, String email);

    void addClassroom(String name, int maxSeats);

    boolean addCourse(String courseId, Teacher teacher, Classroom classroom, String subject, String name, Date startTime, Date endTime);

    Course getCourseById(String name);

    Classroom getClassroomByName(String name);

    Teacher getTeacherById(String id);

    Teacher getTeacherByName(String name);

    Student getStudentById(String id);

    Researcher getResearcherById(String id);

    boolean checkForTimeClash(Course course1, Course course2);

    UniversityMember checkExistId(String type, String id);

    ArrayList<Course> searchClasses(String criteria, String value);

    String[] registerStudent(String courseId, Student student);

    boolean addAvailableAppointmentDate(String teacherId, Date date);

    String[] addAppointment(User makerOfAppointment, UniversityMember madeAppointmentToSee, Date time);

    String listAppointments(String staffId);

    UniversityMember getStaffByName(String name);

    boolean removeMember(String id);
    /*TODO: If create admin create method to remove a student*/
}
