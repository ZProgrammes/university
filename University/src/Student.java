public class Student extends UniversityMember implements User{
    static int studentCount;

    Student(String firstName, String lastName, String address, String email) {
        super(firstName, lastName, address, email);
        // Sets the ID field to SFL1..*QPKE9859
    }

//    Student(String firstName, String lastName) {
//        super(firstName, lastName);
//        // Sets the ID field to SFL1..*
//    }

    @Override
    String createId() {
        studentCount++; // Increase the var. studentCount by one.
        // Sets the id to SFL2 OR TFL2  [T-teacher, S-student, F-fname, L-lname) (Id nums start at 1]
        return "" + 'S' + this.getFirstName().charAt(0) + this.getLastName().charAt(0) + studentCount;
    }

}
