import java.text.SimpleDateFormat;
import java.util.Date;

public class Appointment {
    private int id;
    static int count = 0;
    private User makerOfAppointment;
    private UniversityMember madeAppointmentToSee;
    private Date time;
    private int room;

    public Appointment(User makerOfAppointment, UniversityMember madeAppointmentToSee, Date time, int room) {
        this.makerOfAppointment = makerOfAppointment;
        this.madeAppointmentToSee = madeAppointmentToSee;
        this.time = time;
        this.room = room;

        count++;
        this.id = count; // Set the ID of the appointment to its creation number
}


    public User getMakerOfAppointment() {

        return makerOfAppointment;
    }

    public void setMakerOfAppointment(User makerOfAppointment) {
        this.makerOfAppointment = makerOfAppointment;
    }

    public UniversityMember getMadeAppointmentToSee() {
        return madeAppointmentToSee;
    }

    public void setMadeAppointmentToSee(UniversityMember madeAppointmentToSee) {
        this.madeAppointmentToSee = madeAppointmentToSee;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatDate = new SimpleDateFormat("EEEE d MMMM");
        SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm a");
        
        /*This string is accessed only as html*/
        return  "<br>===========================================================" +
                "<br>Appointment " + getId() +
                "<br>made by <b>" + getMakerOfAppointment().getFirstName() + " " +  getMakerOfAppointment().getLastName() + "</b>" +
                "<br>to see <b>" + getMadeAppointmentToSee().getFirstName() + " " + getMadeAppointmentToSee().getLastName() + "</b>" +
                "<br>on <b>" + formatDate.format(getTime()) + "</b>" +
                "<br>at <b> " + formatTime.format(getTime()) + "</b>" +
                "<br>in room <b>" + getRoom() + "</b>" +
                "<br>===========================================================" +
                "<br>";

    }
}
