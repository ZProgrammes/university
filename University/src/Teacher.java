import java.util.ArrayList;
import java.util.Date;

public class Teacher extends UniversityMember{
    static int teacherCount;
    private String title;
    private int maxClasses;
    private int courseCount;
    private int officeNumber;

    Teacher(String type, String firstName, String lastName, int officeNumber, String address, String email) {
        super(firstName, lastName, address, email);
        this.title = type;
        this.officeNumber = officeNumber;
        if (type.equalsIgnoreCase("professor")) {
            maxClasses = 5;
        } else maxClasses = 10;
    }


    @Override
    String createId() {
        teacherCount++; // Increase the var. studentCount by one.
        // Sets the id to SFL2 OR TFL2  [T-teacher, S-student, F-fname, L-lname) (Id nums start at 1]
        return "" + 'T' + this.getFirstName().charAt(0) + this.getLastName().charAt(0) + teacherCount;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxClasses() {
        return maxClasses;
    }

    public void setMaxClasses(int maxClasses) {
        this.maxClasses = maxClasses;
    }

    public int getCourseCount() {
        return courseCount;
    }

    public void setCourseCount(int courseCount) {
        this.courseCount = courseCount;
    }

    public int getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(int officeNumber) {
        this.officeNumber = officeNumber;
    }
}
