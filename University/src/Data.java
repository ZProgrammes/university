import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Data {
    private University university = University.getInstance();

    public void loadData() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE hh:mm a");

        university.setAdminPassword("Admin");

        //region Lectures | Professors | Researchers staff
        university.addTeacher("Professor", "Heinz", "Doofenshmirtz", 84, "#12 Tri-State, Danville, New York", "heinz@mydoofnetwork.com");

        university.addTeacher("Lecturer", "Jayson", "Mandela", 32, "#1 Jutson St., Carnival Trace, San-Guava", "jasonmandel@gmail.com");
        university.addTeacher("Lecturer", "John", "Elijah", 48, "#26 Elijh St., Hudson Rd., San-Trace", "elijahthe26th@yahoo.com");

        university.addResearcher("Karter", "Cane", 23, "#12 Kingston St., Manhattan", "carterpharaoh@kanenetwork.egypt");
        university.addResearcher("Royal", "Kingsly", 30, "221B Barker St., New York", "kingroyal@highretek.com");
        //endregion

        //region Students note: Id's are created by using 'S' +  the 1st letter of the 1st and last names + the creation number
        /*ID: SHB1*/
        university.addStudent("Hudson", "Belpois", "#13 Bellview Road, 96th Avenu, California", "superbel@school.org");
        /*ID: SCC2*/
        university.addStudent("Carla", "Conundrum", "4598 Mount Tabor, West Nyack ,New York", "carladrum@h.hat");
        /*ID: SJR3 and so on....*/
        university.addStudent("Joseph", "Robinson", "4313 Edwards Street Robersonville, NC 27871", "JosephFRobinson@teleworm.us");
        university.addStudent("Gina", "Anderson", "3421 James Street Rochester, NY 14621", "GinaFAnderson@armyspy.com");
        university.addStudent("Sylvia", "Lane", "2452 Union Street Seattle, WA 98119", "SylviaRLane@jourrapide.com");
        university.addStudent("Perry", "Lewis", "4908 Chatham Way Reston, MD 20191", "PerryJLewis@teleworm.us");
        /*ID: SCA7*/
        university.addStudent("Carolyn", "Anderson", "3151 Hemlock Lane Harlingen, TX 78550", "CarolynJAnderson@rhyta.com");
        university.addStudent("Frank", "Stanley", "4993 Melm Street East Providence, RI 02914", "FrankMStanley@teleworm.us");
        university.addStudent("Susan", "Williams", "1948 Francis Mine Sacramento, CA 95814", "SusanDWilliams@rhyta.com");
        university.addStudent("Paul", "Farner", "11 Spring Haven Trail Paterson, NJ 07501", "PaulRFarner@dayrep.com");
        university.addStudent("Joshua", "Greene", "3133 Fort Street Greenville, NC 27834", "JoshuaBGreene@jourrapide.com");
        /*ID: SEC12*/
        university.addStudent("Ethyl", "Clark", "193 Goldcliff Circle Washington, DC 20024", "EthylIClark@teleworm.us");
        university.addStudent("Louis", "Wilburn", "3548 Columbia Road Wilmington, DE 19801", "LouisDWilburn@rhyta.com");
        university.addStudent("Hattie", "Alexander", "4365 Earnhardt Drive Louisville, KY 40202", "HattieCAlexander@teleworm.us");
        /*ID: SPJ15*/
        university.addStudent("Paul", "Johnson", "4913 Breezewood Court Wichita, KS 67202", "PaulJJohnson@teleworm.us");
        //extra: university.addStudent("Pearlie", "Muniz", "1556 Todds Lane San Antonio, TX 78225", "PearlieMMuniz@rhyta.com");
        //endregion

        //region Classrooms
        university.addClassroom("B12", 6); // 1
        university.addClassroom("A9", 9); // 1
        university.addClassroom("A3", 9); // 1
        university.addClassroom("A6", 9); // 1
        university.addClassroom("A10", 9); // 1
        university.addClassroom("C6", 12); // 1
        university.addClassroom("B9", 10); // 2
        university.addClassroom("D13", 7); // 1
        university.addClassroom("C12", 10); // 1
        //endregion

        /*
         * areas of expertise:
         *   Data Analysis
         *   Programming
         *   Mathematics
         *   Physics
         *   time clash test: with Joshua Greene (id: SJG11) between PH91: Electromagnetism and Optics(registered) and DA35:Statics with R
         *  above successful.
         * */
        //region Courses
        university.addCourse("DA12", university.getTeacherById("THD1"), university.getClassroomByName("B12"),
                "Data Analysis", "Data Analysis using Pandas", dateFormat.parse("Monday 8:00 AM"), dateFormat.parse("Monday 10:00 AM"));

        university.addCourse("P473", university.getTeacherById("THD1"), university.getClassroomByName("C12"),
                "Programming", "Object Oriented Programming using Java", dateFormat.parse("Wednesday 3:00 PM"), dateFormat.parse("Wednesday 5:00 PM"));


        university.addCourse("PH12", university.getTeacherById("TJM2"), university.getClassroomByName("A9"),
                "Physics", "Quantum Mechanics", dateFormat.parse("Friday 1:00 PM"), dateFormat.parse("Friday 5:00 PM"));

        university.addCourse("PH91", university.getTeacherById("TJM2"), university.getClassroomByName("B9"),
                "Physics", "Electromagnetism and Optics",dateFormat.parse("Thursday 4:00 PM"), dateFormat.parse("Thursday 7:00 PM"));

        university.addCourse("PH35", university.getTeacherById("TJM2"), university.getClassroomByName("A3"),
                "Physics", "Thermal Physics", dateFormat.parse("Wednesday 9:00 AM"), dateFormat.parse("Wednesday 12:00 AM"));

        university.addCourse("M534", university.getTeacherById("TJM2"), university.getClassroomByName("A6"),
                "Mathematics", "Applied Mathematics", dateFormat.parse("Tuesday 8:00 AM"), dateFormat.parse("Tuesday 11:00 AM"));


        university.addCourse("P437", university.getTeacherById("TJE3"), university.getClassroomByName("C6"),
                "Programming", "Programming and Software Engineering Practices", dateFormat.parse("Tuesday 8:00 AM"), dateFormat.parse("Tuesday 12:00 AM"));

        university.addCourse("P897", university.getTeacherById("TJE3"), university.getClassroomByName("B9"),
                "Programming", "Web Scripting", dateFormat.parse("Monday 1:30 PM"), dateFormat.parse("Monday 4:30 PM"));

        university.addCourse("DA35", university.getTeacherById("TJE3"), university.getClassroomByName("D13"),
                "Data Analysis", "Statics with R", dateFormat.parse("Thursday 4:00 PM"), dateFormat.parse("Thursday 8:00 PM"));

        university.addCourse("M465", university.getTeacherById("TJE3"), university.getClassroomByName("A10"),
                "Mathematics", "Introduction to Mathematical Thinking", dateFormat.parse("Friday 4:00 PM"), dateFormat.parse("Friday 7:00 PM"));


        //endregion


        //region Adding Students to Courses.
        university.registerStudent("DA12",university.getStudentById("SHB1"));
        university.registerStudent("DA12",university.getStudentById("SCC2"));
        university.registerStudent("DA12",university.getStudentById("SJR3"));
        university.registerStudent("DA12",university.getStudentById("SGA4"));
        university.registerStudent("DA12",university.getStudentById("SSL5"));
        university.registerStudent("DA12",university.getStudentById("SPL6"));

        university.registerStudent("P473",university.getStudentById("SCA7"));
        university.registerStudent("P473",university.getStudentById("SFS8"));
        university.registerStudent("P473",university.getStudentById("SSW9"));



        university.registerStudent("P437",university.getStudentById("SPF10"));
        university.registerStudent("P437",university.getStudentById("SJG11"));

        university.registerStudent("P897",university.getStudentById("SEC12"));
        university.registerStudent("P897",university.getStudentById("SLW13"));
        university.registerStudent("P897",university.getStudentById("SHA14"));

        university.registerStudent("DA35",university.getStudentById("SPJ15"));
        university.registerStudent("DA35",university.getStudentById("SHB1"));
        university.registerStudent("DA35",university.getStudentById("SCC2"));
        university.registerStudent("DA35",university.getStudentById("SJR3"));
        university.registerStudent("DA35",university.getStudentById("SGA4"));
        university.registerStudent("DA35",university.getStudentById("SSL5"));

        university.registerStudent("M465",university.getStudentById("SCA7"));
        university.registerStudent("M465",university.getStudentById("SFS8"));



        university.registerStudent("PH12",university.getStudentById("SSW9"));
        university.registerStudent("PH12",university.getStudentById("SPF10"));

        university.registerStudent("PH91",university.getStudentById("SJG11"));
        university.registerStudent("PH91",university.getStudentById("SEC12"));

        university.registerStudent("PH35",university.getStudentById("SLW13"));
        university.registerStudent("PH35",university.getStudentById("SHA14"));
        university.registerStudent("PH35",university.getStudentById("SPJ15"));

        university.registerStudent("M534",university.getStudentById("SHB1"));
        university.registerStudent("M534",university.getStudentById("SCC2"));
        //endregion


        //region Available Appointment Dates for all Staff.
        //TODO: include note in GUI that all appointments lasts 1 Hour.(?? ;-\ ).
        SimpleDateFormat appointmentDateFormat = new SimpleDateFormat("EEEE d MMMM hh:mm a");
        university.addAvailableAppointmentDate("THD1", appointmentDateFormat.parse("Friday 7 September 10:00 AM"));
        university.addAvailableAppointmentDate("THD1", appointmentDateFormat.parse("Tuesday 16 October 9:00 AM"));
        university.addAvailableAppointmentDate("THD1", appointmentDateFormat.parse("Monday 3 December 1:00 PM"));

        university.addAvailableAppointmentDate("TJM2", appointmentDateFormat.parse("Monday 19 November 10:00 AM"));
        university.addAvailableAppointmentDate("TJM2", appointmentDateFormat.parse("Wednesday 24 October 3:00 PM"));
        university.addAvailableAppointmentDate("TJM2", appointmentDateFormat.parse("Thursday 4 October 1:30 PM"));

        university.addAvailableAppointmentDate("TJE3", appointmentDateFormat.parse("Wednesday 5 September 2:00 PM"));
        university.addAvailableAppointmentDate("TJE3", appointmentDateFormat.parse("Monday 22 October 10:00 AM"));
        university.addAvailableAppointmentDate("TJE3", appointmentDateFormat.parse("Friday 23 November 10:00 AM"));

        university.addAvailableAppointmentDate("RKC1", appointmentDateFormat.parse("Monday 15 October 3:00 PM"));
        university.addAvailableAppointmentDate("RKC1", appointmentDateFormat.parse("Thursday 27 September 1:00 PM"));
        university.addAvailableAppointmentDate("RKC1", appointmentDateFormat.parse("Monday 19 November 9:00 AM"));

        university.addAvailableAppointmentDate("RRK2", appointmentDateFormat.parse("Tuesday 20 November 8:00 AM"));
        university.addAvailableAppointmentDate("RRK2", appointmentDateFormat.parse("Monday 31 December 4:00 PM"));
        //endregion


        //region the Appointments themselves for all staff members.
        university.addAppointment(new Visitor("Hasher", "Hane", "12 George Street Couva", "maddhatter@mydoofnetwork.com"),
               university.getTeacherById("THD1"), appointmentDateFormat.parse("Friday 7 September 10:00 AM"));

        university.addAppointment(university.getResearcherById("RKC1"),
               university.getTeacherById("THD1"), appointmentDateFormat.parse("Tuesday 16 October 9:00 AM"));



        university.addAppointment(new Visitor("Minshall", "Parker", "29 Street New York", "peter@thespiderverse.man"),
               university.getTeacherById("TJM2"), appointmentDateFormat.parse("Wednesday 24 October 3:00 PM"));



        university.addAppointment(university.getResearcherById("RRK2"),
               university.getTeacherById("TJE3"), appointmentDateFormat.parse("Monday 22 October 10:00 AM"));



        university.addAppointment(new Visitor("Maxwell", "Richards", "14 George Street Couva", "max@whelmed.over"),
               university.getResearcherById("RKC1"), appointmentDateFormat.parse("Thursday 27 September 1:00 PM"));

         university.addAppointment(new Visitor("Jackson", "Bond", "14 Silky Street Danville", "bond@whelmed.under"),
               university.getResearcherById("RKC1"), appointmentDateFormat.parse("Monday 15 October 3:00 PM"));



        university.addAppointment(new Visitor("Lakes", "Summers", "124 Farinham Street Jason Avenue Manhattan", "maddhatter@mydoofnetwork.com"),
                university.getResearcherById("RRK2"), appointmentDateFormat.parse("Tuesday 20 November 8:00 AM"));
        //endregion

    }
}